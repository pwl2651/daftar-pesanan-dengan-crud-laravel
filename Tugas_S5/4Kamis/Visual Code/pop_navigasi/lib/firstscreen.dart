import 'package:flutter/material.dart';
import 'package:pop_navigasi/secondscreen.dart';

class Firstscreen extends StatelessWidget {
  const Firstscreen({super.key});

  Future<void> _navigasiScreen(BuildContext context) async {
    final result =
      await Navigator.push(context, MaterialPageRoute(builder: (context){
        return Secondscreen();
      }));

      ScaffoldMessenger.of(context)
      ..removeCurrentSnackBar()
      ..showSnackBar(SnackBar(content: Text('$result')));
  }

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.amber,
        title: Text('Layar Pertama'),
      ),
      body: Center(
        child: ElevatedButton(
          onPressed: (){
            _navigasiScreen(context);
          }, 
          child: Text('Menuju Layar Kedua')),
      )
    );
  }
}