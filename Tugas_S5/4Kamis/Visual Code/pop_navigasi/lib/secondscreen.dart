import 'package:flutter/material.dart';

class Secondscreen extends StatelessWidget {
const Secondscreen({super.key});

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.deepPurple,
        title: Text('Layar Kedua'),
      ),
      body: Center(
        child: ElevatedButton(
          onPressed: (){
            Navigator.pop(context,'Data dari Layar Kedua');
            }, 
          child: Text('Kembali')
        ),
      ),
    );
  }
}