import 'package:belajar_sqlite/ui/entryform.dart';
import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  var combinasi = Color.fromARGB(255, 50, 200, 200);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('SQLite'),backgroundColor: combinasi,),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add,color: Colors.white,),
        backgroundColor: combinasi,
        tooltip: 'Tambah data',
        onPressed: () async{
          Navigator.push(context, MaterialPageRoute(builder:(context) => EntryForm(),)
          );
        },
      ),
  body: createListView(),
    );
  }
  ListView createListView(){
    return ListView.builder(
      itemCount: 5,
      itemBuilder:(context, index) {
        return Card(
          color: Colors.white,
          elevation: 2.0,
          child: ListTile(
            leading: CircleAvatar(
              backgroundColor: combinasi,
              child: Icon(Icons.people,color: Colors.white70,),
            ),
            title: Text("Nama"),
            subtitle: Text("no telp"),
          ),
        );
      },
    );
  }
}