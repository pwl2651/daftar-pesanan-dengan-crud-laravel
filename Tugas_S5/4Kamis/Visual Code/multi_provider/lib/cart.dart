import 'package:flutter/material.dart';

class Cart with ChangeNotifier{
  int _quantity = 17;

  int get quantity => _quantity;

  set quantity(int value){
    _quantity = value;
    notifyListeners();
  }
}