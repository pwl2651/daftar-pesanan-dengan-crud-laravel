import 'package:flutter/material.dart';
import 'package:multi_provider/cart.dart';
import 'package:multi_provider/money.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: MultiProvider(
        providers: [
          ChangeNotifierProvider(
            create: (context) => Cart(),
          ),
          ChangeNotifierProvider(
            create: (context) => Money()
          ),
        ],
        child: Scaffold(
          floatingActionButton: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Consumer<Money>(
              builder: (context, money, child) => Consumer<Cart>(
                builder: (context, cart, child) => FloatingActionButton(
                onPressed: (){
                  if (money.balance >= 500){
                  cart.quantity += 1;
                  money.balance -= 500;
                  }
                },
                child: Icon(Icons.add),
                backgroundColor: Colors.purple,
                ),
              ),
            ),

            Consumer<Money>(
              builder: (context, money, child) => Consumer<Cart>(
                builder: (context, cart, child) => FloatingActionButton(
                onPressed: (){
                  if (cart.quantity >= 1){
                  cart.quantity -= 1;
                  money.balance += 500;
                  }
                },
                child: Icon(Icons.remove),
                backgroundColor: Colors.purple,
                ),
              ),
            ),
          ],
          ),
          appBar: AppBar(
            title: Text("Multi Provider"),
            backgroundColor: Colors.purple,
          ),
          body: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("Balance"),
                    Container(
                      child: Align(
                        alignment: Alignment.centerRight,
                          child: Consumer<Money>(
                            builder: (context, money, child) => Text(
                              money.balance.toString(),
                              style: TextStyle(
                                color: Colors.purple, fontWeight: FontWeight.w700
                              ),
                            ),
                          ),
                      ),
                      height: 30,
                      width: 150,
                      margin: EdgeInsets.all(5),
                      padding: EdgeInsets.all(5),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        color: Colors.purple[100],
                        border: Border.all(color: Colors.purple,width: 2)
                      ),
                    )
                ],),
                Container(
                      child: Align(
                        alignment: Alignment.centerRight,
                        child:Consumer<Cart>(
                          builder:(context, cart, child) => Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                            Text(
                              "Apple(500) x "+ cart.quantity.toString() +" = ",
                              style: TextStyle(
                                color: Colors.black, fontWeight: FontWeight.w500
                              ),
                            ),
                            Text(
                              (500*cart.quantity).toString(),
                              style: TextStyle(
                                color: Colors.black, fontWeight: FontWeight.w500
                              ),
                            )
                          ],),
                        )
                      ),
                      height: 30,
                      margin: EdgeInsets.all(5),
                      padding: EdgeInsets.all(5),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        color: Colors.purple[100],
                        border: Border.all(color: Colors.black,width: 2)
                      ),
                    )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
