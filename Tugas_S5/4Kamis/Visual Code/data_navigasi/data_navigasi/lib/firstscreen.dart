import 'package:data_navigasi/secondscreen.dart';
import 'package:flutter/material.dart';

class FirstScreen extends StatelessWidget {
  const FirstScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green,
        title: Text('Layar Pertama'),
      ),
      body: Center(
        child: ElevatedButton(
          onPressed: (){
            Navigator.push(context,
            MaterialPageRoute(builder: (context){
              return SecondScreen(data:'Data dari Layar Pertama');
            }));
          }, 
          child: Text('Layar Kedua'),
        ),
      ),
    );
  }
}
