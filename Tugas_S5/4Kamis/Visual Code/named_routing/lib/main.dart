import 'package:flutter/material.dart';
import 'package:named_routing/firstscreen.dart';
import 'package:named_routing/secondscreen.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      initialRoute: '/',
      routes: {
        '/' :(context)=>const FirstScreen(),
        '/second':(context) =>const SecondScreen(),
      },
    );
  }
}
