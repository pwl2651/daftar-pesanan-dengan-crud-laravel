import 'dart:convert';

import 'package:belajar_mysql_flutter/add.dart';
import 'package:belajar_mysql_flutter/edit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:http/http.dart' as http;

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  List _get = [];

  final _lightColors = [
    Colors.amber.shade300,
    Colors.lightGreen.shade300,
    Colors.lightBlue.shade300,
    Colors.orange.shade300,
    Colors.pinkAccent.shade100,
    Colors.tealAccent.shade100,
    Colors.grey.shade600,
  ];

  @override
  void initState() {
    super.initState();
    _getData();
  }

  Future _getData() async {
    try {
      final res = await http.get(
        Uri.parse(
          "http://192.168.1.19/note_app/list.php"
        )
      );
      if (res.statusCode == 200) {
        final data = jsonDecode(res.body);
        setState(() {
          _get = data;
        });
      }
    } catch (e) {
      print (e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Note List'),
      ),
      body: _get.length != 0 
      ? MasonryGridView.count(
        crossAxisCount: 2, 
        itemCount: _get.length,
        itemBuilder:(context, index) {
          return GestureDetector(
            onTap: (){
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => Edit(
                    id: _get[index]['id']
                  )
                )
              );
            },
            child: Card(
              color: _lightColors[index % _lightColors.length],
              child: Container(
                constraints: BoxConstraints(minHeight: (index % 2 + 1 ) * 85),
                padding: EdgeInsets.all(15),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      '${_get[index]['date']}', 
                      style: TextStyle(
                        color: Colors.black),
                    ),
                    SizedBox(height: 10,),
                    Text(
                      '${_get[index]['title']}',
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 20,
                        fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              ),
            ),
          );
        },
      )
      : const Center(
        child: Text("Tidak Ada Data",
          style: TextStyle(color: Colors.white,
            fontSize: 20,
            fontWeight: FontWeight.bold
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.black,
        child: Icon(Icons.add),
        onPressed: (){
          Navigator.push(context, MaterialPageRoute(builder: (context) => Add(),));
        },
        ),
    );
  }
}