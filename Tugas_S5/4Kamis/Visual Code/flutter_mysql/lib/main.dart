import 'package:belajar_mysql_flutter/home.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter + MySql',
      theme: ThemeData(
        primaryColor: Colors.black,
        scaffoldBackgroundColor: Color.fromARGB(60, 200, 200, 255),
        appBarTheme: AppBarTheme(
          backgroundColor: Colors.transparent, elevation: 0
        ),
      ),
      initialRoute: '/',
      routes: {
        '/': (context)=>const Home()
      },
    );
  }
}
