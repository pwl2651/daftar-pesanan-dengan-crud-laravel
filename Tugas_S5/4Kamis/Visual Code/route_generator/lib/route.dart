import 'package:flutter/material.dart';
import 'package:route_generator/firstscreen.dart';
import 'package:route_generator/secondscreen.dart';

class RouteGenerator{
  static Route<dynamic> generateRoute(RouteSettings settings){
    switch (settings.name){
      case '/':
        return MaterialPageRoute(builder: (context) =>const FirstScreen());
      case '/second':
        return MaterialPageRoute(builder: (context) =>const SecondScreen());
        default:
        return _errorRoute();
    }
  }

  static Route<dynamic> _errorRoute(){
    return MaterialPageRoute(
      builder: (context){
        return Scaffold(
          appBar: AppBar(title: Text('Error!')),
          body: const Center(child: Text('Error Page'),),
        );
      }
    );
  }
}