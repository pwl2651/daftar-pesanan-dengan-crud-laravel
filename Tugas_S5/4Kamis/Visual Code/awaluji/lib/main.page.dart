import 'package:awaluji/second.page.dart';
import 'package:flutter/material.dart';

class MainPage extends StatelessWidget{
  const MainPage({super.key});
  
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title:Text('Main Page'),
      ),
      body: Center(
        child: ElevatedButton(onPressed: (){
          Navigator.push(context, 
          MaterialPageRoute(builder: (context){
            return SecondPage();
          }));
        },child: Text('Second Page'),),
      ),
    );
  }
}