import 'package:awaluji/third.page.dart';
import 'package:flutter/material.dart';

class SecondPage extends StatelessWidget{
  const SecondPage({super.key});
  
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title:Text('Second Page'),
      ),
      body: Center(
        child:Column(mainAxisAlignment: MainAxisAlignment.center,
        children: [
        ElevatedButton(onPressed: (){
          Navigator.push(context, MaterialPageRoute(builder: (context){
            return ThirdPage();
          }));
        },child: Text('Third Page')),
        Container(height: 10),
        ElevatedButton(onPressed: (){
          Navigator.pop(context);
        },child: Text('Back')),
        ],),
      ),
    );
  }
}