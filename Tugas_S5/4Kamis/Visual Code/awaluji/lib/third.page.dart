import 'package:flutter/material.dart';

class ThirdPage extends StatelessWidget{
  const ThirdPage({super.key});
  
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title:Text('Third Page'),
      ),
      body: Center(
        child:Column(mainAxisAlignment: MainAxisAlignment.center,
        children: [
        ElevatedButton(onPressed: (){
          Navigator.push(context, MaterialPageRoute(builder: (context){
            return ThirdPage();
          }));
        },child: Text('Third Page')),
        Container(height: 10),
        ElevatedButton(onPressed: (){
          Navigator.pop(context);
        },child: Text('Back')),
        ],),
      ),
    );
  }
}