import 'package:flutter/material.dart';
import 'screens/movie_list_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'UTS',
      home: MovieListScreen(),
      theme: ThemeData(
        primaryColor: Color.fromARGB(255, 97, 15, 9),
      ),
    );
  }
}
