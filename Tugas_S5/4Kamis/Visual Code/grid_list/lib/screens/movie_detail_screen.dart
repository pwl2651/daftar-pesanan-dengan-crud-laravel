import 'package:flutter/material.dart';
import 'package:uts_ferdiansyah/model/movie.dart';

class MovieDetailsScreen extends StatelessWidget {
  final Movie movie;

  MovieDetailsScreen(this.movie);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(movie.title),
        backgroundColor: Color.fromARGB(255, 97, 15, 9),
      ),
      body: Container(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Center(
                  child: Image.network(
                    movie.imageUrl,
                    height: 400, // Adjust the height as needed
                  ),
                ),
                SizedBox(height: 16.0), // Add some space between the image and the card
                Card(
                  elevation: 4.0,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          movie.year.toString(),
                          style: TextStyle(fontSize: 17.0, fontStyle: FontStyle.italic),
                        ),
                        SizedBox(height: 8.0),
                        Text(
                          movie.description,
                          textAlign: TextAlign.justify,
                          style: TextStyle(fontSize: 18.0),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
