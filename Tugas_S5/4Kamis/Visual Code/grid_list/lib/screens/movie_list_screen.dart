import 'package:flutter/material.dart';
import 'package:uts_ferdiansyah/model/movie.dart';
import 'package:uts_ferdiansyah/screens/movie_detail_screen.dart';

class MovieListScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Best Anime Ever'),
        backgroundColor: Color.fromARGB(255, 97, 15, 9),
      ),
      backgroundColor: Color.fromARGB(255, 165, 165, 165),
      body: GridView.builder(
          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            childAspectRatio: 2.5,
          ),
          itemCount: movieList.length,
          itemBuilder: (context, index) {
            Movie movie = movieList[index];
            return Card(
              color: Color.fromARGB(255, 255, 255, 255),
              child: ListTile(
                leading: Image.network(movie.imageUrl),
                title: Text(movie.title, textAlign: TextAlign.right),
                subtitle: Text(movie.year.toString(), textAlign: TextAlign.right),
                trailing: Icon(Icons.arrow_forward_rounded),
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => MovieDetailsScreen(movie)),
                  );
                },
              ),
            );
          }),
    );
  }
}
