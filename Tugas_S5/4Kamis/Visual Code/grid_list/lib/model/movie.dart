class Movie {
  String title;
  String description;
  String imageUrl;
  int year;

  Movie(
      {required this.title,
      required this.description,
      required this.imageUrl,
      required this.year});
}

List<Movie> movieList = [
  Movie(
      title: 'Sousou no Frieren',
      description:
          'During their decade-long quest to defeat the Demon King, the members of the heros party—Himmel himself, the priest Heiter, the dwarf warrior Eisen, and the elven mage Frieren—forge bonds through adventures and battles, creating unforgettable precious memories for most of them..',
      year: 2023,
      imageUrl:
          'https://cdn.myanimelist.net/images/anime/1015/138006.jpg'),
  Movie(
      title: 'Bleach: Sennen Kessen-hen',
      description:
          'Substitute Soul Reaper Ichigo Kurosaki spends his days fighting against Hollows, dangerous evil spirits that threaten Karakura Town. Ichigo carries out his quest with his closest allies: Orihime Inoue, his childhood friend with a talent for healing; Yasutora Sado, his high school classmate with superhuman strength; and Uryuu Ishida, Ichigos Quincy rival.',
      year: 2022,
      imageUrl:
          'https://cdn.myanimelist.net/images/anime/1908/135431.jpg'),
  Movie(
      title: 'Vinland Saga',
      description:
          'Young Thorfinn grew up listening to the stories of old sailors that had traveled the ocean and reached the place of legend, Vinland. Its said to be warm and fertile, a place where there would be no need for fighting—not at all like the frozen village in Iceland where he was born, and certainly not like his current life as a mercenary. War is his home now. Though his father once told him, "You have no enemies, nobody does. There is nobody who its okay to hurt as he grew, Thorfinn knew that nothing was further from the truth.',
      year: 2019,
      imageUrl:
          'https://cdn.myanimelist.net/images/anime/1500/103005.jpg'),
  Movie(
      title: "Oshi no Ko",
      year: 2023,
      description:
          "In the entertainment world, celebrities often show exaggerated versions of themselves to the public, concealing their true thoughts and struggles beneath elaborate lies. Fans buy into these fabrications, showering their idols with undying love and support, until something breaks the illusion. Sixteen-year-old rising star Ai Hoshino of pop idol group B Komachi has the world captivated; however, when she announces a hiatus due to health concerns, the news causes many to become worried.",
      imageUrl:
          'https://cdn.myanimelist.net/images/anime/1812/134736.jpg'),
  Movie(
      title: "Hunter x Hunter",
      year: 2011,
      description:
          "Hunters devote themselves to accomplishing hazardous tasks, all from traversing the world's uncharted territories to locating rare items and monsters. Before becoming a Hunter, one must pass the Hunter Examination—a high-risk selection process in which most applicants end up handicapped or worse, deceased.",
      imageUrl:
          'https://cdn.myanimelist.net/images/anime/1337/99013.jpg'),
  Movie(
      title: "Jujutsu Kaisen",
      year: 2018,
      description:
          "Hidden in plain sight, an age-old conflict rages on. Supernatural monsters known as Curses terrorize humanity from the shadows, and powerful humans known as Jujutsu sorcerers use mystical arts to exterminate them. When high school student Yuuji Itadori finds a dried-up finger of the legendary Curse Sukuna Ryoumen, he suddenly finds himself joining this bloody conflict.",
      imageUrl: 'https://cdn.myanimelist.net/images/manga/3/210341.jpg'),
  Movie(
      title: "Kusuriya no Hitorigoto",
      year: 2023,
      description:
          "In an imperial court in ancient China, it has been a few months since a 17-year-old girl known as Maomao was kidnapped and forced to work as a low-level servant at the emperor's palace. Still, she manages to retain her curious and pragmatic spirit, planning to work until her years of servitude are over. One day, however, she catches wind of the fact that the emperor's two infants have fallen gravely ill. She decides to secretly take action, drawing on her experience as a pharmacist raised in the poor red-light district.",
      imageUrl:
          'https://cdn.myanimelist.net/images/anime/1708/138033.jpg'),
  Movie(
      title: "Zom 100: Bucket List of the Dead",
      year: 2023,
      description:
          "After graduating from a top university with an impressive extracurricular record in the rugby club, Akira Tendou has nailed every step of the way to securing his dream job. On top of that, a beautiful and kind co-worker always brightens his day in the office! Life seems to be going very well for Akira until he slowly realizes that sleepless nights and brutal work are his new reality.",
      imageUrl:
          'https://cdn.myanimelist.net/images/anime/1384/136408.jpg'),
  Movie(
      title: "Jigokuraku",
      year: 2023,
      description:
          "Sentenced to death, ninja Gabimaru the Hollow finds himself apathetic. After leading a blood-soaked life, Gabimaru believes he deserves to die. However, every attempt to execute him inexplicably fails. Finally, Sagiri Yamada Asaemon, a fledgling member of a famed executioner clan, is asked to take Gabimaru's life; yet Sagiri makes no move to kill him as requested.",
      imageUrl:
          'https://cdn.myanimelist.net/images/anime/1075/131925.jpg'),
  Movie(
      title: "Watahsi no Shiawase na Kekkon",
      year: 2023,
      description:
          "Misery seems everlasting in Miyo Saimori's life. Born from an arranged marriage, she was quickly discarded after her mother's tragic death. Her father remarried, and her younger half-sister Kaya received all the affection, while Miyo was degraded to a lowly servant. Lacking the strength to fight against her family's abuse, Miyo loses hope that her luck will ever turn.",
      imageUrl:
          'https://cdn.myanimelist.net/images/anime/1147/122444.jpg'),
  Movie(
      title: "Bungou Stray Dogs 5",
      year: 2001,
      description:
          "The Armed Detective Agency is still on the run from the Hunting Dogs, but not all hope is lost. Detective Ranpo Edogawa has a plan to prove the Agency's innocence and save the world from chaos: to find and capture Kamui—the leader of the terrorist organization Decay of the Angel.",
      imageUrl:
          'https://cdn.myanimelist.net/images/anime/1161/136691.jpg'),
  Movie(
      title: "Kimetsu no Yaiba: Katanakaji no Sato-hen",
      year: 2023,
      description:
          "For centuries, the Demon Slayer Corps has sacredly kept the location of Swordsmith Village a secret. As the village of the greatest forgers, it provides Demon Slayers with the finest weapons, which allow them to fight night-crawling fiends and ensure the safety of humans. After his sword was chipped and deemed useless, Tanjirou Kamado, along with his precious little sister Nezuko, is escorted to the village to receive a new one.",
      imageUrl:
          'https://cdn.myanimelist.net/images/anime/1765/135099.jpg')
];
