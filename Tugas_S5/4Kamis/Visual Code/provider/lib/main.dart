import 'package:ClientProvider/application_color.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: ChangeNotifierProvider<ApplicationColor>(
        create: (context) => ApplicationColor(),
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.black,
            title: Consumer<ApplicationColor>(
              builder:(context, applicationColor, child) => Text(
                "Provider State Management",
                style: TextStyle(color: applicationColor.warnaSlider),
                ),
            ),
          ),
          body: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Consumer<ApplicationColor>(
                  builder:(context, applicationColor, child) => AnimatedContainer(
                    width: 100,
                    height: 100,
                    color: applicationColor.warnaSlider,
                    duration: Duration(milliseconds: 500),
                    margin: EdgeInsets.all(5),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(margin:EdgeInsets.all(5),child: Text("AB")),
                    Consumer<ApplicationColor>(
                      builder:(context, applicationColor, child) => Switch(
                        value: applicationColor.isLightBlue, 
                        onChanged: (newValue){
                          applicationColor.isLightBlue = newValue;
                        }
                        ),
                    ),
                    Container(margin:EdgeInsets.all(5),child: Text("LB"))
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
