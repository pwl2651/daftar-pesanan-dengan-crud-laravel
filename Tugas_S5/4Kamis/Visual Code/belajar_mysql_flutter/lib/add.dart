import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class Add extends StatefulWidget {
  const Add({super.key});

  @override
  State<Add> createState() => _AddState();
}

class _AddState extends State<Add> {
  final _formKey = GlobalKey<FormState>();

  var nama = TextEditingController();
  var desk = TextEditingController();

  Future _onSubmit()async{
    try {
      return await http.post(
        Uri.parse("http://192.168.1.19/sql_tugas/create.php"),
        body: {
          "nama": nama.text,
          "desk": desk.text,
        }
      ).then((value) {
        var data = jsonDecode(value.body);
        print(data["message"]);
        Navigator.of(context).pushNamedAndRemoveUntil('/', (route) => false);
      },);
    } catch (e) {
      print(e);
    } 
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Create New Note"),
      ),
      body:Form(
        key:_formKey,
        child: Container(
          padding: EdgeInsets.all(20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Nama Buah',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                )
              ),
              SizedBox(height: 5,),
              TextFormField(
                controller: nama,
                decoration: InputDecoration(
                  hintText: "Nama Buah",
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  fillColor: Colors.white,
                  filled: true,
                ),
                style: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: 16,
                ),
                validator: (value){
                  if (value!.isEmpty) {
                    return "Nama Buah is Required";
                  }
                  return null;
                },
              ),
              SizedBox(height: 20,),
              Text('Deskripsi',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 16,
                  fontWeight: FontWeight.w500
                ),
              ),
              SizedBox(height: 5,),
              TextFormField(
                controller: desk,
                keyboardType: TextInputType.multiline,
                minLines: 5,
                maxLines: null,
                decoration: InputDecoration(
                  hintText: "Deskripsi Data",
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  fillColor: Colors.white,
                  filled: true,
                ),
                style: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: 16,
                ),
                validator: (value){
                  if (value!.isEmpty) {
                    return "Deskripsi Data is Required";
                  }
                  return null;
                },
              ),
              SizedBox(height: 15.0,),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15)
                  ),
                ),
                child: Text(
                  "Submit",
                  style: TextStyle(color: Colors.white),
                  ), 
                onPressed: () {
                  if (_formKey.currentState!.validate()) {
                    _onSubmit();
                  }
                },
              )
            ],
          ),
        ),
      )
          );
        }
      }