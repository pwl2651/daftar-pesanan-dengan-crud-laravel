import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class DetailBuahPage extends StatelessWidget {
  final String fruit;
  final String description;
  final String imageUrl;

  DetailBuahPage({
    required this.fruit,
    required this.description,
    required this.imageUrl,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(fruit, style: GoogleFonts.poppins()),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Image.network(
              imageUrl,
              height: 200.0,
              width: double.infinity,
              fit: BoxFit.cover,
            ),
            SizedBox(height: 16.0),
            Text(
              'Deskripsi $fruit :',
              style: TextStyle(
                fontSize: 18.0,
                fontWeight: FontWeight.bold,
                fontFamily: 'Montserrat',
              ),
            ),
            SizedBox(height: 8.0),
            Text(
              description,
              style: TextStyle(fontFamily: 'Montserrat'),
            ),
          ],
        ),
      ),
    );
  }
}