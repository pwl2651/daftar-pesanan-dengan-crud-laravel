import 'package:flutter/material.dart';
import 'package:uts_pcs_putrawira/data/detailBuah.dart';

class DataBuah extends StatelessWidget {
  final Map<String, Map<String, String>> fruitData = {
    'Apel': {
      'description': 'Apel adalah buah yang berasal dari pohon apel (Malus domestica). Buah ini memiliki kulit yang berwarna merah, hijau, atau kuning, dan daging buah yang keras dan manis. Apel memiliki beberapa biji di dalamnya.',
      'imageUrl': 'assets/images/apel.jpg',
    },
    'Pisang': {
      'description': 'Pisang adalah buah tropis yang berasal dari Asia Tenggara. Buah ini memiliki bentuk memanjang dan berwarna kuning, hijau, atau merah. Pisang memiliki daging buah yang lembut dan manis.',
      'imageUrl': 'assets/images/pisang.jpg',
    },
    'Jeruk': {
      'description': 'Jeruk adalah buah yang berasal dari pohon jeruk (Citrus sinensis). Buah ini memiliki kulit yang berwarna kuning, oranye, atau merah, dan daging buah yang manis dan berair. Jeruk memiliki beberapa biji di dalamnya.',
      'imageUrl': 'assets/images/jeruk.jpg',
    },
    'Anggur': {
      'description': 'Anggur adalah buah yang berasal dari pohon anggur (Vitis vinifera). Buah ini memiliki kulit yang berwarna hijau, merah, atau ungu, dan daging buah yang manis dan berair. Anggur memiliki beberapa biji di dalamnya.',
      'imageUrl': 'assets/images/anggur.jfif',
    },
  };

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Text(
            'Fruit List',
            style: TextStyle(
              fontSize: 24.0,
              fontWeight: FontWeight.bold,
              fontFamily: 'Montserrat',
            ),
          ),
        ),
        ListView.builder(
          shrinkWrap: true,
          itemCount: fruitData.length,
          itemBuilder: (context, index) {
            final fruit = fruitData.keys.elementAt(index);
            final description = fruitData[fruit]!['description'];
            final imageUrl = fruitData[fruit]!['imageUrl'];

            return ListTile(
              title: Text(
                fruit,
                style: TextStyle(fontFamily: 'Montserrat'),
              ),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => DetailBuahPage(
                      fruit: fruit,
                      description: description!,
                      imageUrl: imageUrl!,
                    ),
                  ),
                );
              },
            );
          },
        ),
      ],
    );
  }
}
