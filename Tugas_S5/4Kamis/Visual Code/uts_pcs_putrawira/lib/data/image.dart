import 'package:flutter/material.dart';

class ImageHeader extends StatelessWidget {
  const ImageHeader({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 200.0,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/images/fractal.png'),
          fit: BoxFit.cover,
        ),
      ),
      child: Center(
        child: Text(
          'Buah Lokal',
          style: TextStyle(
            fontSize: 28.0,
            color: Colors.white,
            fontFamily: 'Poppins',
          ),
        ),
      ),
    );
  }
}