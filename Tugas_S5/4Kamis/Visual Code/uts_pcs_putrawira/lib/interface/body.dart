import 'package:flutter/material.dart';
import 'package:uts_pcs_putrawira/data/buah.dart';
import 'package:uts_pcs_putrawira/data/image.dart';

class BodyContent extends StatelessWidget {
  const BodyContent({super.key});

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          ImageHeader(),
          DataBuah(),
        ],
      ),
    );
  }
}