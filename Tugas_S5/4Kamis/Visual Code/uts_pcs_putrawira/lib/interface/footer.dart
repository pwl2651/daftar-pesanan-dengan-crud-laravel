import 'package:flutter/material.dart';

class FooterContent extends StatelessWidget {
  const FooterContent({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60.0,
      color: Colors.blue,
      child: Center(
        child: Text(
          'About Us | © 2023 Fruits Paradise',
          style: TextStyle(
            color: Colors.white,
            fontFamily: 'Montserrat',
          ),
        ),
      ),
    );
  }
}