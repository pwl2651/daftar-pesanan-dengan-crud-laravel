import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:uts_pcs_putrawira/interface/body.dart';
import 'package:uts_pcs_putrawira/interface/footer.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Fruits Home',style: GoogleFonts.poppins())
      ),
      body: BodyContent(),
      bottomNavigationBar: FooterContent(),
    );
  }
}