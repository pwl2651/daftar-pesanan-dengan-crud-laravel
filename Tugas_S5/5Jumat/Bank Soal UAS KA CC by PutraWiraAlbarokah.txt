Nama : Putra Wira Albarokah
NPM : 011210023

Materi : Komputasi Awan

Soal Pilihan Ganda

1. Pengertian komputasi awan adalah…
A. Sebuah model komputasi yang menyediakan sumber daya komputasi, penyimpanan, dan
jaringan yang dapat diakses secara fleksibel dan on-demand dari Internet.
B. Sebuah model komputasi yang menyediakan sumber daya komputasi, penyimpanan, dan
jaringan yang dapat diakses secara terbatas dan on-demand dari Internet.
C. Sebuah model komputasi yang menyediakan sumber daya komputasi, penyimpanan, dan
jaringan yang dapat diakses secara eksklusif dan on-demand dari Internet.
D. Sebuah model komputasi yang menyediakan sumber daya komputasi, penyimpanan, dan
jaringan yang dapat diakses secara gratis dan on-demand dari Internet.
E. Sebuah model komputasi yang menyediakan sumber daya komputasi, penyimpanan, dan
jaringan yang dapat diakses secara tidak terbatas dan on-demand dari Internet.

Jawaban : A

2. Salah satu karakteristik utama komputasi awan adalah:
A. Skalabilitas
B. Kelayakan
C. Keandalan
D. Keamanan
E. Semua jawaban benar

Jawaban : E

3. Model komputasi awan yang menyediakan infrastruktur komputasi, seperti server,
penyimpanan, dan jaringan, adalah:
A. Infrastructure as a Service (IaaS)
B. Platform as a Service (PaaS)
C. Software as a Service (SaaS)
D. Data as a Service (DaaS)
E. Semua salah

Jawaban : A

4. Manakah dari berikut ini yang bukan merupakan karakteristik utama dari model layanan
komputasi awan "Platform as a Service" (PaaS)?
A. Kemampuan untuk mengelola infrastruktur dan sistem operasi.
B. Fokus pada pengembangan dan implementasi aplikasi.
C. Akses ke platform pengembangan dan runtime.
D. Otomatisasi penyebaran aplikasi.
E. Skalabilitas yang mudah.

Jawaban : A

5. Apa yang dimaksud dengan "vendor lock-in" dalam konteks komputasi awan?
A. Keamanan tinggi yang diimplementasikan oleh penyedia layanan awan.
B. Ketergantungan yang sulit dihindari terhadap satu penyedia layanan awan.
C. Kemampuan untuk mengganti penyedia layanan dengan mudah.
D. Integrasi yang sempurna antara layanan dari beberapa penyedia awan.
E. Biaya yang sangat rendah untuk beralih antar penyedia layanan.

Jawaban: B.

True/False

6. Komputasi awan hanya tersedia untuk perusahaan besar.

Jawaban : Salah

7. Komputasi awan mengharuskan pengguna untuk memiliki pengetahuan mendalam tentang
infrastruktur fisik yang mendasarinya.

Jawaban : Salah

8. Function as a Service (FaaS) membebaskan pengguna dari kebutuhan untuk mengelola
infrastruktur, fokus pada kode aplikasi.

Jawaban : Benar

9. Redundansi data pada tingkat geografis yang berbeda adalah salah satu strategi untuk
meningkatkan ketersediaan layanan komputasi awan.

Jawaban : Benar

10. Keamanan data pada komputasi awan sepenuhnya bergantung pada penyedia layanan dan
tidak memerlukan tindakan pengguna.

Jawaban : Salah

Soal Essay

1. Sebutkan 5 Resiko Cloud Computing!

Jawaban : 1. Service Level, 2. Privacy, 3. Compliance, 4. Data Ownership, 5. Data Mobility

2. Componen penting dalam cloud computing ada 2 hal yaitu? Jelaskan!

Jawaban : 1. Front End : Terletak pada sisi pengguna atau client, 2. Back end : terletak pada
sisi sistem jaringan cloud / vendor penyedia layanan cloud computing.

3. Dalam mengelolah biaya, kita harus membandingkan dua model biaya. Antara lain? Dan
jelaskan!

Jawaban : 1) Beban usaha (membayar per bulan, per pengguna untuk setiap layanan). 2)
Modal investasi (membayar biaya beli ditambah pemeliharaan tahunan untuk perangkat lunak
yang berada dalam organisasi Anda - sebagai pengguna).

4. Sebutkan 4 Alasan penggunaan Cloud Computing!

Jawaban : 1. Efektivitas biaya, 2. Bisnis digital, 3. Kemanan data, 4. Penyimpanan file

5. Sebutkan 5 syarat sebagai layanan cloud computing

Jawaban : 1. Layanan bersifat "On Demand", 2. Layanan bersifat elastis/scalable, 3. Layanan
sepenuhnya dikelola oleh penyedia/provider, 4. Sumber Daya Terkelompok ( Resource
pooling ), 5. Akses Pita Lebar