from flask import Flask, render_template, request, redirect, url_for

app = Flask(__name__)

users_data = {'admin': 'admin'}


@app.route('/')
def login():
    return render_template('login.html')


@app.route('/index', methods=['POST'])
def home():
    username = request.form.get('username')
    password = request.form.get('password')

    if username in users_data and users_data[username] == password:
        return render_template('index.html', username=username)
    else:
        return redirect(url_for('login'))


# Daftar anggota perpustakaan sementara
anggota_perpustakaan = []

@app.route('/index')
def index():
    return render_template('index.html', anggota=anggota_perpustakaan)

@app.route('/tambah_anggota', methods=['POST'])
def tambah_anggota():
    nama = request.form.get('nama')
    anggota_perpustakaan.append(nama)
    return render_template('result.html', nama=nama)


@app.route('/logout')
def logout():
    return redirect(url_for('login'))


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)
