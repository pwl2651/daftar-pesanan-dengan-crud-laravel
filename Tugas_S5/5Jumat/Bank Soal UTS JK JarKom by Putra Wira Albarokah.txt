Nama : Putra Wira Albarokah
NPM : 011210023

Mata Kuliah : Jaringan Komputer

Soal Pilihan Ganda

1. Manakah dari berikut yang bukan merupakan protokol transport dalam model OSI?
a. TCP
b. UDP
c. IP
d. SCTP
e. SMTP

Jawaban: c.

2. Apa fungsi dari DNS (Domain Name System) dalam jaringan komputer?
a. Mengamankan jaringan
b. Menghubungkan perangkat keras
c. Mengonversi nama domain ke alamat IP
d. Mengontrol akses pengguna
e. Mengelola bandwidth jaringan

Jawaban: c.

3. Apa yang dimaksud dengan topologi jaringan "ring"?
a. Semua perangkat terhubung ke satu pusat
b. Setiap perangkat terhubung ke dua perangkat lainnya, membentuk lingkaran
c. Semua perangkat terhubung secara acak
d. Setiap perangkat terhubung ke satu perangkat pusat
e. Tidak ada jawaban yang benar

Jawaban: b.

4. Apa perbedaan antara switch dan hub dalam jaringan komputer?
a. Switch dapat memutuskan koneksi, sementara hub tidak dapat
b. Hub dapat bekerja pada lapisan data link, sedangkan switch pada lapisan transport
c. Switch hanya mengirim data ke perangkat yang dituju, sementara hub mengirim ke semua
perangkat
d. Switch tidak dapat mengelola kecepatan transfer data
e. Semua jawaban benar

Jawaban: c.

5. Apa yang dimaksud dengan alamat MAC (Media Access Control) dalam konteks jaringan
komputer?
a. Alamat IP unik dari setiap perangkat dalam jaringan
b. Alamat fisik unik yang terkait dengan kartu jaringan setiap perangkat
c. Alamat server DNS yang digunakan oleh perangkat
d. Alamat gateway default dalam jaringan
e. Alamat loopback untuk uji koneksi

Jawaban: b.

True/False

6. VLAN (Virtual Local Area Network) memungkinkan pembagian fisik yang sama menjadi
beberapa jaringan logis.

Jawaban: True

7. Firewall dapat beroperasi pada lapisan transport dan lapisan aplikasi dalam model OSI.

Jawaban: True


8. UDP (User Datagram Protocol) menyediakan pengiriman data yang andal dan terjamin.

Jawaban: False

9. Algoritma routing OSPF (Open Shortest Path First) menggunakan metode link-state.

Jawaban: True

10. Protokol HTTPS menggunakan enkripsi SSL/TLS untuk menjaga keamanan komunikasi
web.

Jawaban: True