<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Login - Tangeries</title>

  <link rel="stylesheet" type="text/css" href="edit_tambah.css">
  <link rel="icon" type="image/x-icon" href="foto/moonlight.png">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
  <!-- Ikon -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
</head>
<body class="hold-transition login-page bg6">
<div class="login-box">
  <div class="login-logo">
    <a href="index.php" style="font-family:fontKepala;font-size:48px;color:lime;text-shadow: 0 0 5px #00ffff;">
    Welcome, User!</a>
  </div>
  <!-- /.login-logo -->
  <div class="Dengah">
    <div class="">
      <p class="ketengah" style="font-family:fontTeks;font-size:28px;">Sign In To Continue</p>

    <form action="login.php" method="post" class="">
        <div class="input-group mb-3">
          <input type="text" name="usn" class="form-control" placeholder="username">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user-lock"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" name="password" class="form-control" placeholder="password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-8">
            <div class="icheck-primary">
              <input type="checkbox" id="remember">
              <label for="remember" style="font-family:fontTeks;font-size:18px;">
                Remember Me
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-4">
            <button style="font-family:fontTeks;font-size:18px;" type="submit" class="btn btn-primary btn-block">
              Sign In</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!--Bootstrap-->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
</body>
<footer style="text-align: center; padding: 3px; color: #222222; text-shadow: 0 0 5px #ffffff;">
  <p> Ciptaan : Putra Wira <img src="foto/WIRA.png" class="" style="width: 1.5%;" alt="wira"></p>
</footer>
</html>
