<div class="content-wrapper bg-transparent fTek">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="fKep ketengah wHead">
            <h1 style="font-size:40px;">Data Pelanggan</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="Tengah">
              <div class="card-header fKep">
                <h3 class="card-title wHead">Pelanggan Baru</h3>
                <div class="card-tools">
                    <a href="?p=pelanggan" type="button" class="btn btn-sm btn-primary"><i class="fas fa-arrow-circle-left"></i> Kembali</a>
                </div>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="panggilan/simpan_pelanggan.php" method="post" class="form-horizontal" id="quickForm">
                <div class="card-body">
                  <div class="form-group row">
                    <label for="nopem" class="col-sm-2 col-form-label">Nomor Pelanggan</label>
                    <div class="col-sm-10 col-form-input">
                        <input type="text" name="nopem" class="form-control" id="nopem" placeholder="Contoh : 10xx" minlength="4" required>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="pembeli" class="col-sm-2 col-form-label">Nama</label>
                    <div class="col-sm-10 col-form-input">
                        <input type="text" name="pembeli" class="form-control" id="pembeli" placeholder="Nama Pelanggan" required>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="alamat" class="col-sm-2 col-form-label">Alamat</label>
                    <div class="col-sm-10 col-form-input">
                        <textarea name="alamat" class="form-control" id="alamat" rows="4" placeholder="Alamat Anggota" required></textarea>
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Simpan</button>
                  <button type="reset" class="btn btn-danger">Reset</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>