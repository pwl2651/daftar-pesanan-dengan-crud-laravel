<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\maincore;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('bladebelajar', function(Request $request){
    return view ('belajar')->with('request', $request);
});

Route::get('bladecoba',function(){
    return view ('ujicoba.coba');
});

Route::get('slipgaji', function(Request $req){
    return view ('slipgaji')->with('request', $req);
});

Route::get('lihatcore', [maincore::class, /*nama function =>*/'lihat']);

/*---* ======================[ Router Lamo ]===================== *---

Route::get('Belajar', function() {
    echo "Selamat belajar MK Pemrograman Web Lanjutan";
});

Route::get('Hitungan1/{$angka1}/{$angka2}/{$angka3}/{$angka4}', 
            function($angka1,$angka2,$angka3,$angka4) {
    echo "Angka 1 = $angka1<br>";
    echo "Angka 2 = $angka2<br>";
    echo "Angka 3 = $angka3<br>";
    echo "Angka 4 = $angka4<br>";
});

Route::get('Hitungan2',function(){
    $ho2 = 0;
    $angka5 = $_GET["angka5"]?? "$ho2";
    $angka6 = $_GET["angka6"]?? "$ho2";

    echo "Angka Ke-5 = $angka5<br>";
    echo "Angka Ke-6 = $angka6<br>";
});

*/