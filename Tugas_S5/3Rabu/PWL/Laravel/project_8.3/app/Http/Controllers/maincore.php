<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class maincore extends Controller{
    public function lihat(Request $req){
        switch ($req->jabatan) {
            case 'Direktur':
                $gapok = 10000000;
                if($req->harikerja>21){
                    $harigapok = $gapok;
                }else{
                    $harigapok = $gapok - (0.1 * $gapok);
                }
                break;

            default:
                $gapok = 3500000;
                if($req->harikerja>21){
                    $harigapok = $gapok;
                }else{
                    $harigapok = $gapok - (0.1 * $gapok);
                }
                break;
        }

        if ($harigapok > 4000000){
            $pajak = $harigapok * 0.1;
        }else{
            $pajak = $harigapok * 0;
        }

        $total = $harigapok - $pajak;

        return view('slipgaji')
        ->with('req',$req)
        ->with('gapok',$gapok)
        ->with('harigapok',$harigapok)
        ->with('pajak',$pajak)
        ->with('total',$total)
        ;
    }
}
