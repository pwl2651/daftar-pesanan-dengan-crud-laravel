<!DOCTYPE html>
<html lang="en">
<head>
<link rel="stylesheet" type="text/css" href="edit_tambah.css">
</head>
</body class="">
 <div class="main-sidebar sidebar-dark-primary Tor elevation-4" id="pinggiran">
    <a href="homepage.php" class="brand-link">
      <img src="foto/moonlight.png" alt="Trademark Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light fKep" style="color:lime;text-shadow: 0 0 5px #00ffff;">
      Tangeries</span>
    </a>

    <div class="sidebar">
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="dist/img/diri.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info fTek">
          <a href="homepage.php" class="d-block">
            <?php echo $_SESSION['usn']; ?>
          </a>
        </div>
      </div>
      
      <nav class="mt-2 fTek">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">          
          <li class="nav-item">
            <a href="?p=pelanggan" class="nav-link">
              <i class="nav-icon fas fa-user-tie"></i>
              <p>
              Daftar Pelanggan              
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="?p=buah" class="nav-link">
              <i class="nav-icon fas fa-lemon"></i>
              <p>
                Daftar Buah          
              </p>
            </a>
          </li>
          <li class="user-panel nav-item">
            <a href="?p=transaksi" class="nav-link">
              <i class="nav-icon fas fa-address-book"></i>
              <p>
                Transaksi       
              </p>
            </a>
          </li>
          <li class="user-panel nav-item">
            <a href="logout.php" class="nav-link">
              <i class="nav-icon fas fa-sign-out-alt"></i>
              <p>
                Logout                
              </p>
            </a>
          </li>          
        </ul>
      </nav>
    </div>
</div>
</body>
</html>