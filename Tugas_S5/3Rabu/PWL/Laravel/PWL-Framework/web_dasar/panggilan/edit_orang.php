<?php 
require_once('config.php');
$id = mysqli_real_escape_string($link,trim($_GET["id"]));
$que = mysqli_query($link,"SELECT * FROM pembeli WHERE id='$id'");

if(mysqli_num_rows($que)>0){
    $data = mysqli_fetch_array($que);
?>

<div class="content-wrapper fTek bg-transparent">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="ketengah fKep wHead">
            <h1 style="font-size:40px;">Data Pelanggan</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid bg-transparent">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card Tengah">
              <div class="card-header fKep hList">
                <h3 class="card-title">Update Data Pelanggan</h3>
                <div class="card-tools">
                    <a href="?p=pelanggan" type="button" class="btn btn-sm btn-primary"><i class="fas fa-arrow-circle-left"></i> Kembali</a>
                </div>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="panggilan/proses_edit_orang.php" method="post" class="form-horizontal" id="quickForm">
              <input type="hidden" name="id" value="<?=$data['id'];?>">  
              <div class="card-body">
                  <div class="form-group row">
                    <label for="nopem" class="col-sm-2 col-form-label">Nomor Pelanggan</label>
                    <div class="col-sm-10 col-form-input">
                        <input type="text" name="nopem" class="form-control" id="nopem" minlength="4" required value="<?=$data['nopem'] ?>"></input>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="pembeli" class="col-sm-2 col-form-label">Nama</label>
                    <div class="col-sm-10 col-form-input">
                        <input type="text" name="pembeli" class="form-control" id="pembeli" required value="<?=$data['pembeli'] ?>"></input>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="alamat" class="col-sm-2 col-form-label">Alamat</label>
                    <div class="col-sm-10 col-form-input">
                        <textarea name="alamat" class="form-control" id="alamat" rows="4" placeholder="Alamat Anggota" required><?=$data['alamat'] ?></textarea>
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Simpan</button>
                  <button type="reset" class="btn btn-danger">Reset</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <?php 
}else{
    echo '<script>alert("Data Pelanggan Tidak Ditemukan!");window.location="http://localhost/new_iguess/home.php?p=anggota";</script>';
}
  ?>