<div class="content-wrapper bg-transparent fTek">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="ketengah fKep wHead">
            <h1 style="font-size:40px;">Data Buah</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card Tengah">
              <div class="card-header fKep">
                <h3 class="card-title fKep wHead">Data Buah Baru</h3>
                <div class="card-tools">
                    <a href="?p=buah" type="button" class="btn btn-sm btn-primary"><i class="fas fa-arrow-circle-left"></i> Kembali</a>
                </div>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="panggilan/simpan_buah.php" method="post" class="form-horizontal" id="quickForm">
                <div class="card-body">
                  <div class="form-group row">
                    <label for="nobu" class="col-sm-2 col-form-label">Nomor Buah</label>
                    <div class="col-sm-10 col-form-input">
                        <input type="text" name="nobu" class="form-control" id="nobu" placeholder="Contoh : 20xx" minlength="4" required>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="buah" class="col-sm-2 col-form-label">Nama Buah</label>
                    <div class="col-sm-10 col-form-input">
                        <input type="text" name="buah" class="form-control" id="buah" placeholder="Nama Buah" required>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="jumlah" class="col-sm-2 col-form-label">Jumlah/kg</label>
                    <div class="col-sm-10 col-form-input">
                        <input type="number" name="jumlah" class="form-control" id="buah" placeholder="Jumlah/kg" required>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="harga" class="col-sm-2 col-form-label">Harga/kg</label>
                    <div class="col-sm-10 col-form-input">
                        <input type="number" name="harga" class="form-control" id="harga" placeholder="Contoh : 10000" required>
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Simpan</button>
                  <button type="reset" class="btn btn-danger">Reset</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>