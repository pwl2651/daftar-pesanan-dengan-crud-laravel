<?php
session_start();
require_once("panggilan/config.php");
$username = mysqli_real_escape_string($link,trim($_POST["usn"]));
$password = mysqli_real_escape_string($link,trim($_POST["password"]));

if(!empty($username) && !empty($password)){
    // username dan password diisi
    $sql = "SELECT usn,password FROM akun WHERE usn='$username'";
    $result = mysqli_query($link, $sql);
    if(mysqli_num_rows($result) === 1){
        //data ada
        list($nama,$hashed_password)=mysqli_fetch_array($result);
        if(password_verify($password,$hashed_password)){
            $_SESSION['login'] = true;
            $_SESSION['usn'] = $nama;
            //username dan password cocok
            echo "<script>alert('Login Berhasil !');window.location.href='homepage.php';</script>";    
        }else{
            //password salah
            echo "<script>alert('Password Tidak Cocok !');window.location.href='index.php';</script>";    
        }
    }else{
        //data tidak ada
        echo "<script>alert('Username Tidak Ditemukan !');window.location.href='index.php';</script>";    
    }
}else{
    // username dan password kosong
    echo "<script>alert('Username/Password Tidak Boleh Kosong !');window.location.href='index.php';</script>";
}
?>