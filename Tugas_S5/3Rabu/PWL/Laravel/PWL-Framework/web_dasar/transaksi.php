  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper bg-transparent fTek">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="ketengah fKep wHead">
            <h1 style="font-size:40px;">Daftar Transaksi</h1>
          </div>          
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card Tengah">
              <div class="card-header ketengah fKep">
                <h3 class="card-title fKep hList">Nota Transaksi</h3>
                <div class="card-tools">
                  <a href="?p=anggota&act=addbeli" type="button" class="btn btn-sm btn-primary"><i class="fas fa-user-plus"></i> Tambah Daftar</a>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body" type="hidden">
                <table id="example1" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                    <th>No Transaksi</th>
                    <th>No Pelanggan</th>
                    <th>No Buah</th>
                    <th>Jumlah</th>
                    <th>Harga</th>
                    <th>Sub Total</th>
                    <th>Aksi</th>
                  </tr>
                  </thead>
                  <tbody type="">
                    <?php
                    include_once 'panggilan/config.php';
                    $query = mysqli_query($link, "SELECT * FROM transaksi");
                    if(mysqli_num_rows($query)>0){
                    while ($data = mysqli_fetch_array($query)){
                    ?>
                  <tr>
                    <td><?php echo $data['notra']?></td>
                    <td><?php echo $data['nopem']?></td>
                    <td><?php echo $data['nobu']?></td>
                    <td><?php echo $data['jumlah']?></td>
                    <td><?php echo $data['harga']?></td>
                    <td><?php echo $data['sub']?></td>
                    <td>
                      <a href="?p=anggota&act=editbeli&id=<?= $data['id']; ?>" type="button" class="btn btn-sm btn-primary"><i class="fas fa-edit"></i> Edit</a> 
                      <a href="panggilan/hapus_jual.php?id=<?= $data['id']; ?>" type="button" class="btn btn-sm btn-danger" onclick="return confirm('Hapus Data Transaksi <?php echo $data['notra'] ?>?')"><i class="fas fa-trash"></i> Hapus</a>
                    </td>
                  </tr>                  
                  <?php }}else{ ?> 
                  <tr>
                    <td class="ketengah" colspan="7">Data Tidak Ada!</td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->