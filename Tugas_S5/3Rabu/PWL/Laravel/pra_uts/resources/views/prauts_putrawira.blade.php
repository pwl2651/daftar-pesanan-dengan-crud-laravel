<!DOCTYPE html>
<html lang="id">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Kasir</title>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>
<body>

  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
      <a class="navbar-brand" href="#">Kasir</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav">
          <a class="nav-link active" href="#">Home</a>
          <a class="nav-link" href="#">About</a>
          <a class="nav-link" href="#">Contact</a>
        </div>
      </div>
    </div>
  </nav>

  <main class="container">
    <h1>Kasir</h1>

    <div class="row">
      <div class="col-md-6">
        <h2>Data Pelanggan</h2>
        <form action="" method="post">
          <div class="mb-3">
            <label for="nama_pelanggan" class="form-label">Nama Pelanggan</label>
            <input value={{$request->nama}}>
          </div>
        </form>
      </div>
      <div class="col-md-6">
        <h2>Data Barang</h2>
        <div class="card">
          <div class="card-body">
            <h5 class="card-title">Ikan Kecil</h5>
            <p class="card-text">Harga: Rp 10.000</p>
            <input type="number" class="form-control" id="harga_ikan_kecil" value={{$request->jumlahkecil}}>
          </div>
        </div>
        <div class="card">
          <div class="card-body">
            <h5 class="card-title">Ikan Sedang</h5>
            <p class="card-text">Harga: Rp 35.000</p>
            <input type="number" class="form-control" id="harga_ikan_sedang" value={{$request->jumlahsedang}}>
          </div>
        </div>
        <div class="card">
          <div class="card-body">
            <h5 class="card-title">Ikan Besar</h5>
            <p class="card-text">Harga: Rp 78.000</p>
            <input type="number" class="form-control" id="harga_ikan_besar" value={{$request->jumlahbesar}}>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-6">
        <h2>Potongan Harga</h2>
        <div class="mb-3">
          <label for="potongan_harga_ikan_kecil" class="form-label">Potongan Harga Ikan Kecil</label>
          <input type="number" class="form-control" id="potongan_harga_ikan_kecil" value={{$diskonkecil}} readonly>
        </div>
        <div class="mb-3">
          <label for="potongan_harga_ikan_sedang" class="form-label">Potongan Harga Ikan Sedang</label>
          <input type="number" class="form-control" id="potongan_harga_ikan_sedang" value={{$diskonsedang}} readonly>
        </div>
        <div class="mb-3">
          <label for="potongan_harga_ikan_besar" class="form-label">Potongan Harga Ikan Besar</label>
          <input type="number" class="form-control" id="potongan_harga_ikan_besar" value={{$diskonbesar}} readonly>
        </div>
      </div>

      <div class="col-md-6">
        <h2>Total Pembayaran</h2>
        <div class="mb-3">
          <label for="total_harga">Total Harga</label>
          <input type="text" class="form-control" id="total_harga" value={{$total}} readonly>
        </div>
        <div class="mb-3">
          <label for="pajak_penjualan">Pajak Penjualan</label>
          <input type="text" class="form-control" id="pajak_penjualan" value={{$pajak}} readonly>
        </div>
        <div class="mb-3">
          <label for="total_setelah_pajak">Total Setelah Pajak</label>
          <input type="text" class="form-control" id="total_setelah_pajak" value={{$totalakhir}} readonly>
        </div>
      </div>
    </div>
  </main>

  <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>
