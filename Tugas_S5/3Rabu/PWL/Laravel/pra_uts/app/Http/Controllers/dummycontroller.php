<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class dummycontroller extends Controller{
    public function kalkulasi(Request $request){
        $ikankecil = 10000;
        $ikansedang = 35000;
        $ikanbesar = 78000;

        switch ($request->$jumlahkecil) {
            case $jumlahkecil>50:
                $hargakecil = ($ikankecil * $jumlahkecil);
                $diskonkecil = ($ikankecil * $jumlahkecil)*0.05;
                $kecilakhir = $hargakecil - $diskonkecil ;
                break;
            
            case $jumlahkecil>100:
                $hargakecil = ($ikankecil * $jumlahkecil);
                $diskonkecil = ($ikankecil * $jumlahkecil)*0.1;
                $kecilakhir = $hargakecil - $diskonkecil ;
                break;
            
            default:
                $kecilakhir = ($ikankecil * $jumlahkecil);
                break;
        }

        switch ($request->$jumlahsedang) {
            case $jumlahsedang>50:
                $hargasedang = ($ikansedang * $jumlahsedang);
                $diskonsedang = ($ikansedang * $jumlahsedang)*0.04;
                $sedangakhir = $hargasedang - $diskonsedang ;
                break;
            
            case $jumlahsedang>100:
                $hargasedang = ($ikansedang * $jumlahsedang);
                $diskonsedang = ($ikansedang * $jumlahsedang)*0.08;
                $sedangakhir = $hargasedang - $diskonsedang ;
                break;
            
            default:
                $sedangakhir = ($ikansedang * $jumlahsedang);
                break;
        }

        switch ($request->$jumlahbesar) {
            case $jumlahbesar>50:
                $hargabesar = ($ikanbesar * $jumlahbesar);
                $diskonbesar = ($ikanbesar * $jumlahbesar)*0.03;
                $besarakhir = $hargabesar - $diskonbesar ;
                break;

            case $jumlahbesar>100:
                $hargabesar = ($ikanbesar * $jumlahbesar);
                $diskonbesar = ($ikanbesar * $jumlahbesar)*0.06;
                $besarakhir = $hargabesar - $diskonbesar ;
                break;
            
            default:
                $besarakhir = ($ikanbesar * $jumlahbesar);
                break;
        }

        $total = $kecilakhir + $sedangakhir + $besarakhir;
        $pajak = $total * 0.11;
        $totalakhir = $total + $pajak;

        return view('prauts_putrawira')
        ->with('request',$request)
        ->with('ikankecil',$jumlahkecil)
        ->with('ikansedang',$jumlahsedang)
        ->with('ikanbesar',$jumlahbesar)
        ->with('diskonkecil',$diskonkecil)
        ->with('diskonsedang',$diskonsedang)
        ->with('diskonbesar',$diskonbesar)
        ->with('kecilakhir',$kecilakhir)
        ->with('sedangakhir',$sedangakhir)
        ->with('besarakhir',$besarakhir)
        ->with('total',$total)
        ->with('pajak',$pajak)
        ->with('totalakhir',$totalakhir)
        ;
    }
}
