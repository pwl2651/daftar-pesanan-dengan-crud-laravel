<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class main_prauts_putrawira extends Controller{
    public function kalkulasi(Request $request){
        $ikankecil = 10000;
        $ikansedang = 35000;
        $ikanbesar = 78000;

        switch ($request->jumlahkecil) {
            case $request->jumlahkecil>50:
                $hargakecil = ($ikankecil * $request->jumlahkecil);
                $diskonkecil = ($ikankecil * $request->jumlahkecil)*0.05;
                $kecilakhir = $hargakecil - $diskonkecil ;
                break;
            
            case $request->jumlahkecil>100:
                $hargakecil = ($ikankecil * $request->jumlahkecil);
                $diskonkecil = ($ikankecil * $request->jumlahkecil)*0.1;
                $kecilakhir = $hargakecil - $diskonkecil ;
                break;
            
            default:
                $kecilakhir = ($ikankecil * $request->jumlahkecil);
                $diskonkecil = 0;
                break;
        }

        switch ($request->jumlahsedang) {
            case $request->jumlahsedang>50:
                $hargasedang = ($ikansedang * $request->jumlahsedang);
                $diskonsedang = ($ikansedang * $request->jumlahsedang)*0.04;
                $sedangakhir = $hargasedang - $diskonsedang ;
                break;
            
            case $request->jumlahsedang>100:
                $hargasedang = ($ikansedang * $request->jumlahsedang);
                $diskonsedang = ($ikansedang * $request->jumlahsedang)*0.08;
                $sedangakhir = $hargasedang - $diskonsedang ;
                break;
            
            default:
                $sedangakhir = ($ikansedang * $request->jumlahsedang);
                $diskonsedang = 0;
                break;
        }

        switch ($request->jumlahbesar) {
            case $request->jumlahbesar>50:
                $hargabesar = ($ikanbesar * $request->jumlahbesar);
                $diskonbesar = ($ikanbesar * $request->jumlahbesar)*0.03;
                $besarakhir = $hargabesar - $diskonbesar ;
                break;

            case $request->jumlahbesar>100:
                $hargabesar = ($ikanbesar * $request->jumlahbesar);
                $diskonbesar = ($ikanbesar * $request->jumlahbesar)*0.06;
                $besarakhir = $hargabesar - $diskonbesar ;
                break;
            
            default:
                $besarakhir = ($ikanbesar * $request->jumlahbesar);
                $diskonbesar = 0;
                break;
        }

        $total = $kecilakhir + $sedangakhir + $besarakhir;
        $pajak = $total * 0.11;
        $totalakhir = $total + $pajak;

        return view('prauts_putrawira')
        ->with('request',$request)
        ->with('diskonkecil',$diskonkecil)
        ->with('diskonsedang',$diskonsedang)
        ->with('diskonbesar',$diskonbesar)
        ->with('kecilakhir',$kecilakhir)
        ->with('sedangakhir',$sedangakhir)
        ->with('besarakhir',$besarakhir)
        ->with('total',$total)
        ->with('pajak',$pajak)
        ->with('totalakhir',$totalakhir)
        ;

    }
}
