<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\main_prauts_putrawira;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('prauts_putrawira', function (Request $request) {
    return view('prauts_putrawira')->with('request', $request);
});

Route::get('inti', [main_prauts_putrawira::class, /*nama function =>*/'kalkulasi']);

// Route::get('prauts_putrawira', function (Request $request) {
//     return view('prauts_putrawira')->with('request', $request);
// });

// Route::get('inti', [dummycontroller::class, /*nama function =>*/'kalkulasi']);