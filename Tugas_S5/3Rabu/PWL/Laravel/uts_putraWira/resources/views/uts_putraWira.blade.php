<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Web Dashboard</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,700&display=swap">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:400,700&display=swap">
    <style>
        body {
            font-family: 'Montserrat', sans-serif;
        }

        header {
            font-family: 'Poppins', sans-serif;
            padding: 20px;
            background-color: #333;
            color: white;
        }

        nav {
            background-color: #555;
            padding: 10px;
        }

        nav a {
            color: white;
            text-decoration: none;
            margin: 0 10px;
        }

        .main-content {
            padding: 20px;
        }

        footer {
            background-color: #333;
            color: white;
            text-align: center;
            padding: 10px;
        }
    </style>
</head>

<body>

    <header>
        <h1>Web Dashboard</h1>
    </header>

    <nav>
        <a href="#">Home</a>
        <a href="#">Profile</a>
        <a href="#">Settings</a>
    </nav>

    <div class="main-content">
        <h2>Data Pekerja</h2>
        <table class="table">
            <tbody>
                <tr>
                    <th>NIP</th>
                    <td>{{$req->NIP}}</td>
                </tr>
                <tr>
                    <th>Nama</th>
                    <td>{{$req->nama}}</td>
                </tr>
                <tr>
                    <th>Email</th>
                    <td>{{$req->email}}</td>
                </tr>
                <tr>
                    <th>Divisi</th>
                    <td>{{$req->divisi}}</td>
                </tr>
                <tr>
                    <th>Jumlah Anak</th>
                    <td>{{$req->anak}}</td>
                </tr>
                <tr>
                    <th>Gaji Pokok Pekerjaan : {{$req->divisi}}</th>
                    <td>{{$gapok}}</td>
                </tr>
                <tr>
                    <th>Tunjangan</th>
                    <td>{{$tunjangan}}</td>
                </tr>
                <tr>
                    <th>Pajak : {{$persenpajak}}</th>
                    <td>{{$pajak}}</td>
                </tr>
                <tr>
                    <th>Total Gaji</th>
                    <td>{{$gajiakhir}}</td>
                </tr>
            </tbody>
        </table>
    </div>

    <footer>
        <p>About Us</p>
    </footer>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"></script>
</body>

</html>
