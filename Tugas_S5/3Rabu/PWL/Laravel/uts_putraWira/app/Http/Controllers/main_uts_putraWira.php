<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class main_uts_putraWira extends Controller{
    public function kalkulasi(Request $req){

        $tunjangan = 0;
        $pajak = 0;
        $persenpajak = "0%";

        switch ($req->divisi) {
            case 'Produksi':
                $gapok = 2200000;
                break;
            case 'Promosi':
                $gapok = 1500000;
                break;
            case 'Distribusi':
                $gapok = 1200000;
                break;
            case 'Gudang':
                $gapok = 1700000;
                break;
            case 'Security':
                $gapok = 1400000;
                break;
            case 'Service':
                $gapok = 2400000;
                break;
            /* ↓↓↓ Untuk Test Pajak ↓↓↓ */
            case 'COO':
                $gapok = 11000000 ;
                break;
            /* ^^^ Untuk Test Pajak ^^^ */
            default:
                $gapok = 0;
        }

        if($req->anak>2) {
            $tunjangan = 0.1 * $gapok;
        }else if ($req->anak>0 && $req->anak<3){
            $tunjangan = 0.05 * $gapok;
        }

        if ($gapok>10000000) {
            $pajak = $gapok * 0.1 ;
            $persenpajak = "10%";
        }else if ($gapok>5000000){
            $pajak = $gapok * 0.05 ;
            $persenpajak = "5%"; 
        }
        
        $gajiakhir = $gapok + $tunjangan - $pajak;

        return view('uts_putraWira')
        ->with('req',$req)
        ->with('gapok',$gapok)
        ->with('tunjangan',$tunjangan)
        ->with('pajak',$pajak)
        ->with('persenpajak',$persenpajak)
        ->with('gajiakhir',$gajiakhir)
        ;
    }
}
