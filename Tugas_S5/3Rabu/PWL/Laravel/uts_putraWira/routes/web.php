<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use App\Http\Controllers\main_uts_putraWira;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('uts_putrawira', function (Request $req) {
    return view('uts_putraWira')->with('request', $req);
});

Route::get('UTS_PutraWira', [main_uts_putraWira::class, /*nama function =>*/'kalkulasi']);
