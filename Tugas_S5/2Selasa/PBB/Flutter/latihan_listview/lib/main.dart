import 'package:flutter/material.dart';

void main() {
  runApp(ListViewPage());
}

class ListViewPage extends StatelessWidget {
  ListViewPage({super.key});
  var negara =[
    'Indonesia', 'Thailand', 'Amerika Serikat', 'Kanada', 'Jepang', 'Korea Selatan', 'Korea Utara', 'Rusia',
    'Inggris', 'Jerman'
  ];
  @override
  Widget build(BuildContext context) {
    // return MaterialApp(
    //   home: Scaffold(
    //     body: ListView(
    //       children: [
    //         for(var i=0;i<negara.length;i++)
    //           Center(
    //             child: Container(
    //               padding: EdgeInsets.all(5),
    //               margin: EdgeInsets.only(bottom: 5),
    //               decoration: BoxDecoration(
    //                 border: Border.all(width: 1)
    //               ),
    //               child: Text(negara[i])),
    //           ),
    //       ],
    //     ),
    //   ),
    // );
    return MaterialApp(
      home: Scaffold(
        body: ListView.builder(
            padding: EdgeInsets.all(8),
            itemCount: negara.length,
            itemBuilder: (BuildContext context,int index) {
              return Container(
                margin: EdgeInsets.all(5),
                decoration: BoxDecoration(
                  color: index % 2 == 0 ? Colors.blue : Colors.red,
                  border: Border.all(width: 1)
                ),
                height: 50,
                child: Center(child: Text('${negara[index]}')),
              );
            }
          ),
        ),
      );
  }
}

// if(mahasiswa[index[2]=="Laki-laki"]){
// }