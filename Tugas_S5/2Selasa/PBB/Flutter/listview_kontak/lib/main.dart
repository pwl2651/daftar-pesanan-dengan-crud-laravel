import 'package:flutter/material.dart';

void main() {
  runApp(ListViewPage());
}

class ListViewPage extends StatelessWidget {
  ListViewPage({super.key});
  var mahasiswa = [
    ['011210023','Putra Wira Albarokah','Laki-laki'],
    ['01210011','Devi','Perempuan'],
    ['01210012','Shella','Perempuan'],
    ['01210013','Taufiq','Laki-laki'],
    ['01210014','Raven','Laki-laki'],
    ['01210015','Windi','Perempuan'],
    ['01210016','Regita','Perempuan'],
    ['01210017','Reza','Laki-laki'],
    ['01210018','Fitri','Perempuan'],
    ['01210019','Nadhif','Laki-laki'],
    ['01210020','Nadia','Perempuan'],
    ['01210021','Dina','Perempuan'],
    ['01210022','Pero','Laki-laki'],
    ['01210023','Naufal','Laki-laki']
    ];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: ListView.builder(
          itemCount: mahasiswa.length,
          itemBuilder: (BuildContext context,int index) {
            return Container(
                margin: EdgeInsets.only(left: 10,right: 10, bottom: 1,top: 10),
                decoration: BoxDecoration(
                  color: mahasiswa[index][2]=='Laki-laki' ? Colors.blue : Colors.pink,
                  border: Border.all(width: 1)
                ),
                height: 50,
                    
                child: Center(child: Text('${mahasiswa[index]}')),
            );
          },
        ),
      ),
    );
  }
}
