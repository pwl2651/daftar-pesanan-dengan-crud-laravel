import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Color.fromARGB(255, 0, 255, 255),
          title: Text('Biodata Diri',
          style: TextStyle(
            fontSize: 25,
            color: Color.fromARGB(255, 0, 0, 0)
        ),),),
        backgroundColor: Color.fromARGB(255, 255, 255, 255),
        body: Column(
          children: [
            Container(
              alignment: Alignment.center,
              margin: EdgeInsets.all(20),
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                border: Border.all(
                  width: 10,
                  color: Color.fromARGB(255, 0, 255, 255)
                )
              ),
              child: Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(20), 
                    child:Text("""Biodata Diri
==============
Nama  : Putra Wira Albarokah
NPM   : 011210023""",
                  style: TextStyle(
                    fontSize: 25,
                    color: Color.fromARGB(255, 0, 0, 0),
                  ),),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(20),
                    child: Text("""Kontak
========
No. HP  : 0895640636xxx
Alamat  : Jalan AKBP H. Umar
E-Mail  : personalputra06@gmail.com""",
                  style: TextStyle(
                    fontSize: 25,
                    color: Color.fromARGB(255, 0, 0, 0),
                    ),),
                  )
                ],
              ),
            ),

            Container(
              alignment: Alignment.center,
              margin: EdgeInsets.all(20),
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                border: Border.all(
                  width: 10,
                  color: Color.fromARGB(255, 0, 255, 255)
                )
              ),
              child: Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(20), 
                    child:Text("""Biodata Diri
==============
Nama  : Putra Wira Albarokah
NPM   : 011210023""", 
                  style: TextStyle(
                    fontSize: 25,
                    color: Color.fromARGB(255, 0, 0, 0),
                    ),),
                    ),
                  Padding(
                    padding: const EdgeInsets.all(20),
                    child: Text("""Kontak
========
No. HP  : 0895640636xxx
Alamat  : Jalan AKBP H. Umar
E-Mail  : personalputra06@gmail.com""",style: TextStyle(
                    fontSize: 25,
                    color: Color.fromARGB(255, 0, 0, 0),
                    ),),
                    ),
                ],
              ),
            )
          ],
        ),
      )
    );
  }
}



// import 'package:flutter/material.dart';

// class MainApp extends StatelessWidget {
//   const MainApp({super.key});

//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//       home: Scaffold(
//         backgroundColor: Color.fromARGB(255, 0, 255, 255),
//         body: Column(
//           children: [
//             Container(
//               alignment: Alignment.center,
//               margin: EdgeInsets.all(20),
//               padding: EdgeInsets.all(10),
//               decoration: BoxDecoration(
//                 border: Border.all(
//                   width: 10,
//                   color: Colors.black
//                 )
//               ),
//               child: Column(
//                 children: [
//                   Padding(padding: EdgeInsets.all(10),
//                   child: Text(
//                     'TPillar',
//                     style: TextStyle(
//                       fontSize: 25,
//                       color: Color.fromARGB(255, 0, 0, 0),
//                     ),
//                   ),
//                   ),
//                   Text(
//                     '3145135325',
//                   )
//                 ],
//               ),
//             ),

//             Container(
//               alignment: Alignment.center,
//               margin: EdgeInsets.all(20),
//               padding: EdgeInsets.all(10),
//               decoration: BoxDecoration(
//                 border: Border.all(
//                   width: 10,
//                   color: Colors.black
//                 )
//               ),
//               child: Column(
//                 children: [
//                   Padding(padding: EdgeInsets.all(10),
//                   child: Text(
//                     'TPillar',
//                     style: TextStyle(
//                       fontSize: 25,
//                       color: Color.fromARGB(255, 0, 0, 0),
//                     ),
//                   ),
//                   ),
//                   Text(
//                     '3145135325',
//                   )
//                 ],
//               ),
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }