import 'package:flutter/material.dart'; 
 
void main() { 
  runApp(GridViewPage()); 
} 
 
class GridViewPage extends StatelessWidget { 
  @override 
  Widget build(BuildContext context) { 
    return MaterialApp( 
      home: Scaffold( 
        body: GridView.count( 
          primary: false, 
          padding: const EdgeInsets.all(20), 
          crossAxisSpacing: 5, 
          mainAxisSpacing: 10, 
          crossAxisCount: 2, 
          scrollDirection: Axis.horizontal,
          children: <Widget>[ 
            Container( 
              padding: const EdgeInsets.all(8), 
              color: Colors.teal[100], 
              child: Image.asset('images/engiegame.jpg'), 
            ), 
            Container( 
              padding: const EdgeInsets.all(8), 
              color: Colors.teal[200], 
              child: Image.asset('images/hemat-uang.jpg'), 
            ), 
            Container( 
              padding: const EdgeInsets.all(8), 
              color: Colors.teal[300], 
              child: Image.asset('images/mahtakut.jpg'), 
            ), 
            Container( 
              padding: const EdgeInsets.all(8), 
              color: Colors.teal[400], 
              child: Image.asset('images/mamahtakut.jpg'),  
            ), 
            Container( 
              padding: const EdgeInsets.all(8), 
              color: Colors.teal[500], 
              child: Image.asset('images/sus-the-rock.gif'),  
            ), 
            Container( 
              padding: const EdgeInsets.all(8), 
              color: Colors.teal[600], 
              child: Image.asset('images/Waduh.jpg'),  
            ), 
          ], 
        ), 
      ), 
    ); 
  } 
}