import 'package:flutter/material.dart';
import 'package:latihan_navigasi/screen2.dart';

class Screen1 extends StatelessWidget {
  const Screen1({super.key});

  @override
    Widget build(BuildContext context) { 
    return MaterialApp( 
      home: Scaffold( 
        body: GridView.count( 
          primary: false, 
          padding: const EdgeInsets.all(20), 
          crossAxisSpacing: 5, 
          mainAxisSpacing: 10, 
          crossAxisCount: 2, 
          scrollDirection: Axis.horizontal,
          children: <Widget>[ 
              GestureDetector(
                child: Container( 
                  padding: const EdgeInsets.all(8), 
                  color: Colors.teal[100], 
                  child: Image.asset('images/engiegame.jpg'), 
                ),
              onTap: (){
                Navigator.push(context, 
                MaterialPageRoute(builder: (context)=>
                Screen2(gambar:'engiegame.jpg')
              ));
              } 
            ),
            GestureDetector(
              child: Container( 
                padding: const EdgeInsets.all(8), 
                color: Colors.teal[200], 
                child: Image.asset('images/hemat-uang.jpg'), 
              ),
              onTap: (){
                Navigator.push(context, 
                MaterialPageRoute(builder: (context){
                  return Screen2(gambar:'hemat-uang.jpg');
                }));
              } 
            ), 
            GestureDetector(
              child: Container( 
                padding: const EdgeInsets.all(8), 
                color: Colors.teal[300], 
                child: Image.asset('images/mahtakut.jpg'), 
              ),
              onTap: (){
                Navigator.push(context, 
                MaterialPageRoute(builder: (context){
                  return Screen2(gambar:'mahtakut.jpg');
                }));
              } 
            ), 
            GestureDetector(
              child: Container( 
                padding: const EdgeInsets.all(8), 
                color: Colors.teal[400], 
                child: Image.asset('images/mamahtakut.jpg'),  
              ),
              onTap: (){
                Navigator.push(context, 
                MaterialPageRoute(builder: (context){
                  return Screen2(gambar:'mamahtakut.jpg');
                }));
              } 
            ), 
            GestureDetector(
              child: Container( 
                padding: const EdgeInsets.all(8), 
                color: Colors.teal[500], 
                child: Image.asset('images/sus-the-rock.gif'),  
              ),
              onTap: (){
                Navigator.push(context, 
                MaterialPageRoute(builder: (context){
                  return Screen2(gambar:'sus-the-rock.gif');
                }));
              } 
            ), 
            GestureDetector(
              child: Container( 
                padding: const EdgeInsets.all(8), 
                color: Colors.teal[600], 
                child: Image.asset('images/Waduh.jpg'),  
              ),
              onTap: (){
                Navigator.push(context, 
                MaterialPageRoute(builder: (context){
                  return Screen2(gambar:'Waduh.jpg');
                }));
              } 
            ), 
          ], 
        ), 
      ), 
    ); 
  } 
}