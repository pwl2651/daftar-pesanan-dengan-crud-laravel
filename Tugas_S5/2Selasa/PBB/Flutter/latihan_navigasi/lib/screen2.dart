import 'package:flutter/material.dart';

class Screen2 extends StatelessWidget {
  final String gambar;
  Screen2({super.key, required this.gambar});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: Container(
          padding: const EdgeInsets.all(20), 
          child: Column(
            children: [
              Image.asset('images/${gambar}',width: MediaQuery.of(context).size.width * 0.5),
            ],
          ),
        ),
      ),
    );
  }
}