<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\buah;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class dataController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $todos = buah::all();
        if ($todos->count()>0){
            return response()->json([
                'status'=>True,
                'data'=>$todos
            ]);
        }
        return response()->json([
            'status'=>false,
            'message'=>"Data buah masih kosong!"
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //validasi
        $validator = Validator::make($request->all(),[ 
            'nama'=>['required'],
            'jenis'=>['required'],
            'deskripsi'=> ['required']
        ]);

        if ($validator -> fails()){
            return response()->json([
                'status' => false,
                'message'=> $validator->errors()
            ]);
        }


        $todo = new buah();
        $todo -> nama = $request -> nama;
        $todo -> jenis = $request -> jenis;
        $todo -> deskripsi = $request -> deskripsi;
//        $todo -> finished = $request->finished == null ? 0 : $request -> finished;
        if ($todo->save()){
            return response()->json([
                'status'=>true,
                'message'=> "Data buah berhasil ditambahkan!"
            ]);
        }

        //disimpan
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $todo = buah::find($id);

        if ($todo != null){
            return response()->json([
                'status'=>True,
                'data'=>$todo
            ]);
        }
        return response()->json([
            'status'=>false,
            'message'=>"ID buah tidak ditemukan!"
        ]);

    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),[ 
            'nama'=>['required'],
            'jenis'=>['required'],
            'deskripsi'=> ['required']
        ]);

        if ($validator -> fails()){
            return response()->json([
                'status' => false,
                'message'=> $validator->errors()
            ]);
        }
        //update data
        $todo = buah::find($id);
        $todo -> nama = $request -> nama;
        $todo -> jenis = $request ->jenis;
        $todo -> deskripsi = $request -> deskripsi;
        $todo -> finished = $request -> finished;
        if ($todo->save()){
            return response()->json([
                'status'=>true,
                'message'=> "Data buah berhasil di-update!"
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $todo = buah::find($id);

        if ($todo == null){
            return response()->json([
                'status'=> false,
                'message'=>'ID tidak ditemukan!'
            ],404);
        }

        $todo ->delete();
        return response()->json([
            'status' => true,
            'message'=>'Data berhasil dihapus!'
        ],200);
    }
}
