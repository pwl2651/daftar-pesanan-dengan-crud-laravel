<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\api\dataController;
use App\Http\Controllers\Api\userController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

//http://localhost:8000/api/register
Route::post('/register',[userController::class, 'register']);

//http://localhost:8000/api/login
Route::post('/login',[userController::class, 'login']);

Route::middleware('auth:sanctum')->group(function(){
    Route::apiResource('buah', dataController::class);
    //http://localhost:8000/api/logout 
    Route::get('/logout',[userController::class, 'logout']);
});