<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\wira;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class todoController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $todos = wira::all();
        if ($todos->count()>0){
            return response()->json([
                'status'=>True,
                'data'=>$todos
            ]);
        }
        return response()->json([
            'status'=>false,
            'message'=>"Data masih kosong!"
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //validasi
        $Validator = Validator::make($request->all(),[ 
            'title'=>['required'],
            'description'=> ['required']
        ]);

        if ($Validator -> fails()){
            return response()->json([
                'status' => false,
                'message'=> $Validator->errors()
            ]);
        }


        $todo = new wira();
        $todo -> nama = $request -> nama;
        $todo -> description = $request -> description;
        $todo -> is_complete = $request->is_complete == null ? 0 : $request -> is_complete;
        if ($todo->save()){
            return response()->json([
                'status'=>true,
                'message'=> "Data berhasil ditambahkan!"
            ]);
        }

        //disimpan
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $todo = wira::find($id);

        if ($todo == null){
            return response()->json([
                'status'=>false,
                'message'=>"ID data tidak ditemukan!"
            ]);
        }
        return response()->json([
            'status'=>True,
            'data'=>$todo
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(wira $todo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $Validator = Validator::make($request->all(),[ 
            'title'=>['required'],
            'description'=> ['required']
        ]);

        if ($Validator -> fails()){
            return response()->json([
                'status' => false,
                'message'=> $Validator->errors()
            ]);
        }
        //update data
        $todo = wira::find($id);
        $todo -> nama = $request -> nama;
        $todo -> description = $request -> description;
        $todo -> is_complete = $request -> is_complete;
        if ($todo->save()){
            return response()->json([
                'status'=>true,
                'message'=> "Data berhasil di-update!"
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $todo = wira::find($id);

        if ($todo == null){
            return response()->json([
                'status'=> false,
                'message'=>'ID tidak ditemukan!'
            ],404);
        }

        $todo ->delete();
        return response()->json([
            'status' => true,
            'message'=>'Data berhasil dihapus!'
        ],200);
    }
}
