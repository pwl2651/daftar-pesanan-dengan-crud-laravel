<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class userController extends Controller
{
    public function register(Request $request){
        //validasi
        $validator = Validator::make($request->all(),[
            'name'=>['required'],
            'email'=>['required','email','unique:users'],
            'password'=>['required','confirmed']
        ],[
            'name.required'=>'Nama harus diisi!',
            'email.required'=>'Email harus diisi!',
            'email.email'=>'Email invalid! Contoh : nama@email.com',
            'email.unique'=>'Email sudah terdaftar!',
            'password.required'=>'Password harus diisi!',
            'password.confirmed'=>'Password tidak sama!'
        ]);
        if ($validator->fails()){
            return response()->json([
                'status'=>false,
                'errors'=>$validator->errors()
            ],400);
        }
        //simpan user
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->save();

        return response()->json([
            'status'=> true,
            'message'=>'Pembuatan akun baru berhasil!'
        ],201);
    }
    public function login(Request $request){
        //validasi
        $validator = Validator::make($request->all(),[
            'username'=>['required','email'],
            'password'=>['required']
        ],[
            'username.required'=>'Username harus diisi!',
            'username.email'=>'Username menggunakan format email!',
            'password.required'=>'Password harus diisi!'
        ]);

        if ($validator->fails()){
            return response()->json([
                'status'=>false,
                'errors'=>$validator->errors()
            ],400);
        }

        //cek username dan password
        if (!Auth::attempt(['email'=> $request->username, 'password'=> $request->password])){
            return response()->json([
                'status'=>false,
                'message'=>'invalid Username or Password!'
            ],400);
        }

        //generate token
        $token = Auth::user()->createToken('authToken')->plainTextToken;

        return response()->json([
            'status'=>true,
            'user'=>Auth::user(),
            'token'=>$token
        ],200);

    }
    public function logout(){
        Auth::user()->tokens()->delete();

        return response()->json([
            'status'=> true,
            'message'=>'Logout berhasil!'
        ],200);
    }
}
