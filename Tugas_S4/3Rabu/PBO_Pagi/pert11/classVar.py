class laptop:
    def __init__(self,merk,harga,size,warna,jumlah):
        self.merk = merk
        self.harga = harga
        self.size = size
        self.warna = warna
        self.jumlah = jumlah
    
    def tipe(self)->1:
        print(f"Tipe Laptop : {self.merk} {self.harga} {self.size} {self.warna}. Tersedia : {self.jumlah}")

Legion = laptop("Lenovo","Rp 15.000.000,00",'16"',"Hitam",10)
rog = laptop("Asus","Rp 18.000.000,00",'16"',"Hitam",0)

pilihan = {"legion":Legion,"rog":rog}

for i in range(1):
    isi = input("Sebutkan Nama Laptop : ")
    isi = isi.lower()
    while isi not in pilihan:
        isi = input("Invalid! (Gunakan Kapital di awal kata!) : ")
        isi = isi.lower()
    tampil = pilihan[isi]
    if tampil == pilihan["legion"] and Legion.jumlah>0:
        print(laptop.tipe(Legion))
    elif tampil == pilihan["rog"] and rog.jumlah>0:
        print(laptop.tipe(rog))
    else:
        print("Laptop tidak tersedia!")