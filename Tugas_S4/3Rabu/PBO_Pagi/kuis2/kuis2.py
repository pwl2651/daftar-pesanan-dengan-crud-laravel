class car:
    def __init__(self, brand):
        self.brand = brand

    def start(self):
        print("Car started")

class electricCar(car):
    def start(self):
        print("Electric car started")

def main():
    myCar = electricCar("Tesla")
    myCar.start()

main()