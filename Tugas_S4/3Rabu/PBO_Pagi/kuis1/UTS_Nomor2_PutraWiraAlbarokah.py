"""
Putra Wira Albarokah
011210023

Soal Nomor 2
"""

Nip = int(input(f"Masukkan Nomor Induk Pekerja (NIP) Anda: "))
Nama = input(f"Masukkan Nama Anda: ")
Email = input(f"Masukkan alamat email Anda : ")
Divisi = {"Produksi": 2200000.0, "Promosi": 1500000.0, "Distribusi": 1200000.0, 
         "Gudang": 1700000.0, "Security": 1400000.0, "Service": 2400000.0}
print(f"""Divisi: 
    - Produksi
    - Promosi
    - Distribusi
    - Gudang
    - Security
    - Service""")
for i in range(1):
    inputDivisi = input(f"Masukkan asal Divisi: ")
    while inputDivisi not in Divisi:
        inputDivisi = input("Invalid Input (Gunakan huruf kapital di awal kalimat/kata!): ")
    bonusDivisi = Divisi[inputDivisi]
    print(f"")
jumlahAnak = int(input(f"Jumlah anak Anda: "))

print("")
print(f"Nomor Induk Pekerja: {Nip}")
print(f"Nama pekerja: {Nama}")
print(f"Alamat email pekerja: {Email}")
print(f"Divisi {inputDivisi}")
print(f"Jumlah Anak: {jumlahAnak}")

if jumlahAnak >= 3:
    tunjangan1 = bonusDivisi * 0.10
    pajak1 = bonusDivisi * (0.10 - 0.05)
    gaji1 = pajak1 + bonusDivisi
    print(f"Bonus tunjangan {jumlahAnak} anak : {tunjangan1}")
    print(f"Total Gaji Anda setelah dipotong pajak sebesar 5% : {gaji1}")
elif jumlahAnak > 0 and jumlahAnak <= 2:
    tunjangan2 = bonusDivisi * (0.05)
    pajak2 = bonusDivisi * (0.05 - 0.05)
    gaji2 = pajak2 + bonusDivisi
    print(f"Bonus tunjangan {jumlahAnak} anak : {tunjangan2}")
    print(f"Total Gaji Anda setelah dipotong pajak sebesar 5% : {gaji2}")
elif jumlahAnak == 0:
    gaji3 = bonusDivisi
    pajak3 = gaji3 * 0.05
    print(f"Total Gaji Anda setelah dipotong pajak sebesar 5% : {gaji3-pajak3}")
print("")
input("Klik Enter")
print("")