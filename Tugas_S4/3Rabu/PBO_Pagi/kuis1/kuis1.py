"""
Putra Wira Albarokah
011210023
"""

print("Tentukan bangun datar yang ingin dihitung (Pilih dalam bentuk angka)")
while True:
    try:
        bangun = int(input("1. Segitiga\n2. Macam-macam Segi Empat\n: "))
        if bangun not in range(1,3):
            print("Pilih angka yang tertera!")
            continue
        break
    except ValueError:
        print("Masukkan Dalam bentuk angka!")
        continue

if bangun == 1:
    print("Menghitung sebuah segitiga")
    while True:
        try:
            pilihan = int(input("1. Luas\n2. Keliling\n3. Tinggi\n4. Alas\n: "))
            if pilihan not in range(1,5):
                print("Pilih angka yang tertera!")
                continue
            break
        except ValueError:
            print("Masukkan Dalam bentuk angka!")
            continue

    if(pilihan == 1):
        alas = float(input("Masukkan nilai alas : "))
        tinggi = float(input("Masukkan nilai tinggi : "))
        L = alas * tinggi / 2
        hasilLuas = "{:.2f}".format(L)
        print(f"Luas dari segitiga dengan Alas {alas} dan Tinggi {tinggi} adalah {hasilLuas}")
    elif(pilihan == 2):
        alas = float(input("Masukkan nilai alas : "))
        sisi = float(input("Masukkan nilai sisi : "))
        K = alas + sisi + sisi
        hasilKeliling = "{:.2f}".format(K)
        print(f"Hasil keliling segitiga dengan Alas {alas} dan sisi {sisi} adalah {hasilKeliling}")
    elif (pilihan == 3):
        luas = float(input("Masukkan Luas segitiga : "))
        alas = float(input("Masukkan nilai alas : "))
        T = (2 * luas) / alas
        hasilTinggi = "{:.2f}".format(T)
        print(f"Hasil Tinggi segitiga dengan Luas {luas} dan alas {alas} adalah {hasilTinggi}")
    elif(pilihan == 4):
        luas = float(input("Masukkan Luas segitiga : "))
        tinggi = float(input("Masukkan nilai tinggi : "))
        A = (2 * luas) / tinggi
        hasilAlas = "{:.2f}".format(A)
        print(f"Hasil Alas segitiga dengan Luas {luas} dan Tinggi {tinggi} adalah {hasilAlas}")

elif bangun == 2:
    print("Menghitung sebuah bangun datar segi empat")
    while True:
        try:
            pilihan = int(input("Pilih dalam bentuk angka : \n1. Luas Persegi \n2. Keliling Persegi \n3. Luas Persegi Panjang \n4. Luas Jajar Genjang \n5. Luas Trapesium \n6. Luas Belah Ketupat \n7. Luas Layang-Layang: "))
            if pilihan not in range(1,8):
                print("Pilih angka yang tertera!")
                continue
            break
        except ValueError:
            print("Masukkan Dalam bentuk angka!")
            continue

    if(pilihan == 1):
        print("Luas Persegi")
        sisi = float(input("Masukkan nilai sisi Persegi : "))
        L1 = sisi * sisi
        hasilLuas1 = "{:.2f}".format(L1)
        print(f"Luas dari Persegi dengan sisi {sisi} adalah {hasilLuas1}")
    elif(pilihan == 2):
        print("Keliling Persegi")
        sisi = float(input("Masukkan nilai sisi Persegi : "))
        L2 = 4 * sisi
        hasilLuas2 = "{:.2f}".format(L2)
        print(f"Keliling dari Persegi dengan sisi {sisi} adalah {hasilLuas2}")
    elif (pilihan == 3):
        print("Luas Persegi Panjang")
        panjang = float(input("Masukkan Panjang Persegi Panjang : "))
        lebar = float(input("Masukkan Lebar Persegi Panjang : "))
        L3 = panjang * lebar
        hasilLuas3 = "{:.2f}".format(L3)
        print(f"Hasil Luas Persegi Panjang dengan Panjang {panjang} dan Lebar {lebar} adalah {hasilLuas3}")
    elif(pilihan == 4):
        print("Luas Jajar Genjang")
        alas = float(input("Masukkan Alas Jajar Genjang : "))
        tinggi = float(input("Masukkan Tinggi Jajar Genjang : "))
        L4 = alas * tinggi
        hasilLuas4 = "{:.2f}".format(L4)
        print(f"Hasil Luas Jajar Genjang dengan Luas {alas} dan Tinggi {tinggi} adalah {hasilLuas4}")
    elif pilihan == 5:
        print("Luas Trapesium")
        a = float(input("Masukkan Panjang atas : "))
        b = float(input("Masukkan Panjang bawah : "))
        tinggi = float(input("Masukkan Tinggi : "))
        L5 = (a+b) * tinggi / 2
        hasilLuas5 = "{:.2f}".format(L5)
        print(f"Hasil Luas Trapesium dengan panjang atas {a}, panjang bawah {b} dan Tinggi {tinggi} adalah {hasilLuas5}")
    elif pilihan == 6:
        print("Luas Belah Ketupat")
        d1 = float(input("Masukkan Diagonal 1 : "))
        d2 = float(input("Masukkan Diagonal 2 : "))
        L6 = d1 * d2 / 2
        hasilLuas6 = "{:.2f}".format(L6)
        print(f"Hasil Luas Belah Ketupat dengan diagonal 1 {d1} dan diagonal 2 {d2} adalah {hasilLuas6}")
    elif pilihan == 7:
        print("Luas Layang-Layang")
        d1 = float(input("Masukkan Diagonal 1 : "))
        d2 = float(input("Masukkan Diagonal 2 : "))
        L7 = d1 * d2 / 2
        hasilLuas7 = "{:.2f}".format(L7)
        print(f"Hasil Luas Layang-Layang dengan diagonal 1 {d1} dan diagonal 2 {d2} adalah {hasilLuas7}")
        pass