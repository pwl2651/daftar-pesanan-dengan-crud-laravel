"""
Putra Wira Albarokah
011210023
"""

harga = {"A": 5000000.0, "B": 4000000.0, "C": 3000000.0, "D": 2000000.0, "E": 1000000.0, "F": 500000.0}
totalHarga = 0

transaksi = int(input("Jumlah Transaksi yang akan dihitung : "))

hargaSeat = print(f"""Harga Seat : 
            Seat A = {harga["A"]}
            Seat B = {harga["B"]}
            Seat C = {harga["C"]}
            Seat D = {harga["D"]}
            Seat E = {harga["E"]}
            Seat F = {harga["F"]}""")

for i in range(transaksi):
    pesanan = input(f"Pilih Seat Pesanan Transaksi :")
    while pesanan not in harga:
        pesanan = input("Invalid Input")
    hargaBeli = harga[pesanan]
    print("")
    totalHarga +=  hargaBeli

if totalHarga >= 0 and totalHarga <= 10000000:
    pajak1 = totalHarga* 0.10 
    pembayaranAkhir = pajak1 + totalHarga
    print(f"Terkena Pajak 10%, Total Harga : {pembayaranAkhir}")
elif totalHarga>10000000:
    pajak2 =totalHarga * 0.15
    pembayaranAkhir = pajak2 + totalHarga
    print(f"Terkena Pajak 15%, Total Harga : {pembayaranAkhir}")

pembayaran = float(input("Jumlah Pembayaran Pelanggan : "))
pengembalian = print(f"Total Pengembalian {pembayaran - pembayaranAkhir}")

input("Klik Enter")