class Barang:
    def __init__(self, nama, stok, harga):
        self.nama = nama
        self.stok = stok
        self.harga = harga

class Toko:
    def __init__(self):
        self.barang_list = []
        self.jumlah_penjualan = 0
        self.pendapatan = 0

    def tambah_barang(self, nama, stok,harga):
        barang = Barang(nama, stok, harga)
        self.barang_list.append(barang)
        print(f"stok Barang '{nama}' berjumlah {stok} berhasil ditambahkan.")

    def hapus_barang(self, nama):
        for barang in self.barang_list:
            if barang.nama == nama:
                self.barang_list.remove(barang)
                print(f"Barang '{nama}' dihapus.")

    def jual_barang(self, nama, jumlah, pembayaran):
        for barang in self.barang_list:
            if barang.nama == nama:
                if barang.stok >= jumlah:
                    if pembayaran >= jumlah:
                        barang.stok -= jumlah
                        self.jumlah_penjualan += jumlah
                        self.pendapatan += jumlah * barang.harga
                        print(f"Barang '{nama}' terjual sebanyak {jumlah}.")
                        if pembayaran > barang.harga:
                            print(f"kembalian : {pembayaran - barang.harga}")
                        else:
                            pass
                    else:
                        print("Pembayaran tidak cukup.")
                else:
                    print("Stok barang tidak mencukupi.")
                return

        print(f"Barang '{nama}' tidak ditemukan.")

    def tampilkan_list_barang(self):
        if not self.barang_list:
            print("Tidak ada barang yang tersedia.")
        else:
            print("List Barang:")
            for barang in self.barang_list:
                print(f"{barang.nama} - Stok: {barang.stok} - Harga: {barang.harga}")

    def tampilkan_jumlah_penjualan_dan_pendapatan(self):
        print(f"Jumlah Penjualan: {self.jumlah_penjualan}")
        print(f"Pendapatan: {self.pendapatan}")

    def mulai(self):
        while True:
            print("=== Aplikasi Toko ===")
            print("Menu:")
            print("a. Tambah Barang")
            print("b. Hapus Barang")
            print("c. Jual Barang")
            print("d. Tampilkan List Barang")
            print("e. Tampilkan Jumlah Penjualan dan Pendapatan")
            print("f. Keluar dari Program")

            pilihan = input("Pilih menu: ")

            if pilihan == "a":
                nama = input("Masukkan nama barang: ")
                stok = int(input("Masukkan stok barang: "))
                harga = int(input("Masukkan harga barang: "))
                self.tambah_barang(nama, stok, harga)
            elif pilihan == "b":
                nama = input("Masukkan nama barang yang ingin dihapus: ")
                self.hapus_barang(nama)
            elif pilihan == "c":
                nama = input("Masukkan nama barang yang ingin dijual: ")
                jumlah = int(input("Masukkan jumlah barang yang ingin dijual: "))
                pembayaran = int(input("Masukkan jumlah pembayaran: "))
                self.jual_barang(nama, jumlah, pembayaran)
            elif pilihan == "d":
                self.tampilkan_list_barang()
            elif pilihan == "e":
                self.tampilkan_jumlah_penjualan_dan_pendapatan()
            elif pilihan == "f":
                print("Keluar dari program.")
                break
            else:
                print("Pilihan tidak valid.")
                
aplikasi = Toko()
aplikasi.mulai()