"""
Putra Wira Albarokah
011210023
"""

ikan = {"Kecil":10000.0,"Sedang":50000.0,"Besar":100000.0}

ikanKecil = 0
ikanSedang = 0
ikanBesar = 0

jumlahJenis = int(input("Jumlah jenis ikan yang akan dibeli : "))

harga = print(f"""
Harga Ikan : 
    Kecil = {ikan["Kecil"]}
    Sedang = {ikan["Sedang"]}
    Besar = {ikan["Besar"]}
    """)

for i in range(jumlahJenis):
    jenis = input(f"Jenis Ikan {i+1} : ")
    while jenis not in ikan:
        jenis = input("Invalid Input (Gunakan huruf kapital di awal kalimat/kata!): ")
    jenisPilihan = ikan[jenis]
    if jenisPilihan == ikan["Kecil"]:
        ikanKecil = int(input("Jumlah Ikan Kecil : "))
    elif jenisPilihan == ikan["Sedang"]:
        ikanSedang = int(input("Jumlah Ikan Sedang : "))
    elif  jenisPilihan == ikan["Besar"]:
        ikanBesar = int(input("Jumlah Ikan Besar : "))

totalIkan = (ikanKecil + ikanSedang + ikanBesar)

hargaKecil = ikanKecil * ikan["Kecil"]
hargaSedang = ikanSedang * ikan["Sedang"]
hargaBesar = ikanBesar * ikan["Besar"]

if totalIkan > 0 and totalIkan < 5:
    totalSemua = hargaKecil + hargaSedang + hargaBesar
    print(f"""Total harga :

Ikan Kecil : {hargaKecil}
Ikan Sedang : {hargaSedang}
Ikan Besar : {hargaBesar}

Total Harga : {totalSemua}""")
    
elif totalIkan >= 5 and totalIkan <= 10:
    diskonSedang = hargaSedang * 0.03
    totalSedang = hargaSedang - diskonSedang

    diskonBesar = hargaBesar * 0.05
    totalBesar = hargaBesar - diskonBesar

    totalSemua = hargaKecil + totalSedang + totalBesar

    print(f"""Total harga :

Ikan Kecil : {hargaKecil} (tidak ada diskon)
Ikan Sedang : {hargaSedang} + (diskon 3%) = {totalSedang}
Ikan Besar : {hargaBesar} + (diskon 5%) = {totalBesar}

Total Harga : {totalSemua}""")
    
elif totalIkan > 10:
    diskonKecil = hargaKecil * 0.03
    totalKecil = hargaKecil - diskonKecil

    diskonSedang = hargaSedang * 0.05
    totalSedang = hargaSedang - diskonSedang

    diskonBesar = hargaBesar * 0.1
    totalBesar = hargaBesar - diskonBesar

    totalSemua = totalKecil + totalSedang + totalBesar

    print(f"""Total harga :

Ikan Kecil : {hargaKecil} + (diskon 3%) = {totalKecil}
Ikan Sedang : {hargaSedang} + (diskon 5%) = {totalSedang}
Ikan Besar : {hargaBesar} + (diskon 10%) = {totalBesar}

Total Harga : {totalSemua}""")
else:
    print("ERROR!")

print("")
input("Klik Enter")
print("")