class buah: #parent
    ciri1 = "Memiliki biji"
    ciri2 = "memiliki kulit"

class jenis(buah): #child:buah
    ciri3 = "berduri dikulit"
    ciri4 = "Aroma khas"
    ciri5 = "berwarna cerah"
    ciri6 = "biji lebih banyak dari daging"

obj1 = buah()
obj2 = jenis()

print(obj2.ciri1)
print(obj2.ciri2)
print(obj2.ciri3)
print(obj2.ciri4)
print(obj2.ciri5)
print(obj2.ciri6)