class orang: #parent
    __nama = 'Budi'
    umur = 32

    def tampilorang(self):
        print(self.__nama)
        print(self.umur)

class pasangan(orang): #child
    __nama2 = 'Vila'
    umur2 = 30

    def tampilpasangan(self):
        print(self.__nama2)
        print(self.umur2)
    pass

obj1 = orang()
obj2 = pasangan()

#print(obj1.__nama)
print(obj2.tampilorang(),obj2.tampilpasangan())