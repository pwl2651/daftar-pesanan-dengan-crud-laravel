class kucing:
    def suara(self):
        print("Meow!")

    def bulu(self):
        print("Ada yang berbulu lebat, tipis, bahkan tidak ada.")
        # self.tipe1 = print("Berbulu lebat")
        # self.tipe2 = print("Berbulu tipis")
        # self.tipe3 = print("Tidak berbulu")

class anggora(kucing):
    def bulu(self):
        print("Berbulu lebat")

class sphynx(kucing):
    def bulu(self):
        print("Tidak berbulu")

obj1 = kucing()
obj2 = anggora()
obj3 = sphynx()

print(obj1.suara())
print(obj1.bulu())

print(obj2.suara())
print(obj2.bulu())

print(obj3.suara())
print(obj3.bulu())