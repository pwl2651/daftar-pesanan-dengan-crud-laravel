"""

Putra Wira Albarokah
011210023

Buatlah hitungan nilai dengan nested
80 - 100 Nilai A
70 - 80 Nilai B
60 - 70 Nilai C
50 - 60 Nilai D
0 - 50 Nilai E
Mendapat Nilai A,B,C Lulus
Mendapat Nilai D,E tidak Lulus
"""

nilai = float(input("Masukkan Nilai : "))

if nilai >=80 and nilai<=100:
    print(f"Nilai {nilai} : A")
else:
    if nilai >=70 and nilai <80:
        print(f"Nilai {nilai} : B")
    else:
        if nilai >=60 and nilai <70:
            print(f"Nilai {nilai} : C")
        else:
            if nilai >=50 and nilai <60:
                print(f"Nilai {nilai} : D")
            else:
                if nilai >=0 and nilai <50:
                    print(f"Nilai {nilai} : E")
                else:
                    print("Masukkan nilai 0 - 100")

if nilai >= 60 and nilai <=100:
    print("Lulus")
else:
    if nilai >= 0 and nilai <=60:
        print("Tidak Lulus")