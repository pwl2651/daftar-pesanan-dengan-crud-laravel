print("""
Sebuah toko buka jam 8 pagi sampai jam 12 siang
jam 12 siang sampai jam 1 siang ishoma
lalu jam 1 sampai jam 6 toko buka kembali
selain dari pada jam tersebut toko tutup
""")

jamtoko = float(input("Jam Saat ini? "))

if jamtoko >=8 and jamtoko <= 12:
    print(f"Toko Buka Pagi")
elif jamtoko >= 12.01 and jamtoko <= 13:
    print(f"Toko Sedang Ishoma")
elif jamtoko >= 13.01 and jamtoko <= 18:
    print(f"Toko Buka Sore")
elif jamtoko >= 24.01:
    print(f"Planet Mana Itu? Tidak ada jam {jamtoko} di Bumi")
else:
    print(f"Toko Tutup")
