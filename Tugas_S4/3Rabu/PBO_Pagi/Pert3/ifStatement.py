bilangan = int(input("Masukkan Bilangan : "))

if bilangan >= 10 and bilangan <= 99:
    print(f"Bilangan {bilangan} Adalah Puluhan")
elif bilangan >= 1 and bilangan <= 9 :
    print(f"Bilangan {bilangan} Adalah Satuan")
elif bilangan >= 100 and bilangan <= 999:
    print(f"Bilangan {bilangan} Adalah Ratusan")
elif bilangan <= -1:
    print(f"Bilangan {bilangan} Mengarah ke Negatif")
elif bilangan == 0:
    print(f"{bilangan} Adalah Titik Tengah Bilangan")
else:
    print(f"Bilangan {bilangan} Melebihi Batas")

# Uji Coba Sendiri

# numer = int(input("Masukkan Angka : "))

# if(numer == 1):
#     print("Angka Satu")
# elif(numer == 2):
#     print("Angka Dua")
# elif (numer == 3):
#     print("Angka Tiga")
# else:
#     print("Angka Melebihi Batas")

# teger = int(input("Masukkan Angka Kedua : "))

# if (teger > 100):
#     print("Angka Melebihi Batas")
# else:
#     if(teger == 12):
#         print(f"Ada {teger} Bulan Dalam 1 Tahun")
#     else:
#         if(teger == 28):
#             print(f"Ada {teger} Hari Pada Bulan Februari")
#         else:
#             if(teger == 77):
#                 print("NICE!")
#             else:
#                 print(f"Angka {teger}")