import mysql.connector

sql = mysql.connector.connect(
    host = "localhost",
    user="root",
    password="",
    database="db_nilai"
)

my_sql = sql.cursor()

while True:
    pilihan = input("""
Database nilai
1. Insert nilai
2. Tampilkan nilai
3. Delete nilai
                        
Exit
                        
""")
    pilihan.lower()
    if pilihan == "1":
        import pyCreate
    elif pilihan == "2":
        import pyRead
    elif pilihan == "3":
        import pyDelete
    elif pilihan == "exit":
        break
    else:
        print("Invalid Input!")