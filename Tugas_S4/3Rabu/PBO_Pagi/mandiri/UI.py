import mysql.connector

import daftarNama
import detailPesanan
import batal
import check

sql = mysql.connector.connect(
    host = "localhost",
    user="root",
    password="",
    database="putra_db"
)
aktivasi = True

my_sql = sql.cursor()

class dokumen():
    def daftar():
        daftarNama.masukan()

    def detail():
        detailPesanan.detail.run()

    def batal():
        batal.cancel.run()

    def checkOut():
        check.checkOut.run()
        

while aktivasi == True:
    pilihan = input("""
  Pelayanan hotel
===================
1. Daftar Check-In
2. Detail pesanan
3. Check-Out
4. Batal Pesanan
                        
Exit
                        
""")
    pilihan.lower()
    if pilihan == "1" or pilihan == "daftar check-in" or pilihan == "daftar":
        dokumen.daftar()
    elif pilihan == "2" or pilihan == "detail pesanan" or pilihan == "detail":
        dokumen.detail()
    elif pilihan == "3" or pilihan =="check-out" or pilihan == "check":
        dokumen.checkOut()
    elif pilihan == "4" or pilihan == "batal pesanan" or pilihan == "batal":
        dokumen.batal()
    elif pilihan == "exit":
        aktivasi = False
        break
    else:
        print("Invalid Input!")