import mysql.connector

sql = mysql.connector.connect(
    host = "localhost",
    user="root",
    password="",
    database="putra_db"
)

my_sql = sql.cursor()

jenis = {"1":"Pria","2":"Wanita"}
tipeKamar = {"1":"Standar","2":"Deluxe","3":"Suite"}

class DBHandler:
    def __init__(self, nama, umur, jk,nokamar,tipe_kamar,lama_tinggal):
        self.nama = nama
        self.umur = umur
        self.jk = jk
        self.nokamar = nokamar
        self.tipe_kamar = tipe_kamar
        self.lama_tinggal = lama_tinggal

    def daftarNama(self, nama, umur, jk):
        if jk in jenis:
            sqlNama = "INSERT INTO daftarnama (nama, umur, jk) VALUES (%s, %s, %s)"
            values = (nama, umur, jenis[jk])
            my_sql.execute(sqlNama, values)
        else:
            print("Invalid Input!")

    def kamar(self,nokamar,tipe_kamar):
        if tipe_kamar in tipeKamar:
            sqlKamar = "INSERT INTO detail_pesanan (nokamar, tipe_kamar) VALUES (%s, %s)"
            values = (nokamar, tipeKamar[tipe_kamar])
            my_sql.execute(sqlKamar, values)
        else:
            print("Invalid Input!")

    def jadwal(self,lama_tinggal):
            sqlTinggal = "INSERT INTO jadwal_check (lama_tinggal) VALUES (%s)"
            values = (lama_tinggal)
            my_sql.execute(sqlTinggal, (values,))
            sql.commit()
            print("Pelanggan berhasil didaftarkan.")

class masukan(DBHandler):
    def __init__(self):
        while True:
            #detail pelanggan
            nama = input("Masukan Nama Pelanggan : ")
            umur = int(input("Masukan Umur : "))
            for jk_value,jk_name in jenis.items():
                print(f"{jk_value}. {jk_name}")
            jk = input("Jenis Kelamin : ")
            #tipe kamar
            for kamar_value,kamar_name in tipeKamar.items():
                print(f"{kamar_value}. {kamar_name}")
            tipe_kamar = input("Tipe Kamar : ")
            try:
                lama_tinggal = int(input("Lama tinggal (hari) : "))
            except ValueError:
                print("Invalid Input")

            newKamar = 10
            nokamar = newKamar + 1
            db = DBHandler(nama, umur, jk,nokamar,tipe_kamar,lama_tinggal)
            super().__init__(nama, umur, jk, nokamar, tipe_kamar, lama_tinggal)
            db.daftarNama(nama, umur, jk)
            db.kamar(nokamar, tipe_kamar)
            db.jadwal(lama_tinggal)