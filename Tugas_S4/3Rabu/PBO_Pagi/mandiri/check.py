import mysql.connector

sql = mysql.connector.connect(
    host = "localhost",
    user="root",
    password="",
    database="putra_db"
)

my_sql = sql.cursor()

class checkOut():
    def run():
        while True:
            try:
                optOut = int(input("Masukan idjadwal pelanggan untuk check-out (ketik 0 untuk kembali): "))
                if optOut == 0:
                    break
                ended = "UPDATE jadwal_check SET finished = 1 WHERE idjadwal = %s"

                my_sql.execute(ended, (optOut,))

                sql.commit()

                print(my_sql.rowcount, f"id {optOut} Berhasil Check-Out")
                input("Klik enter untuk kembali")
                break
            except ValueError:
                print("Invalid Input")