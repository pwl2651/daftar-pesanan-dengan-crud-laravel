-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 24, 2023 at 03:48 AM
-- Server version: 10.4.28-MariaDB
-- PHP Version: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `putra_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `daftarnama`
--

CREATE TABLE `daftarnama` (
  `idnama` int(5) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `umur` int(3) NOT NULL,
  `jk` enum('Pria','Wanita') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `detail_pesanan`
--

CREATE TABLE `detail_pesanan` (
  `iddetail` int(5) NOT NULL,
  `nokamar` int(3) NOT NULL,
  `tipe_kamar` enum('Standar','Deluxe','Suite') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Stand-in structure for view `informasi`
-- (See below for the actual view)
--
CREATE TABLE `informasi` (
`idnama` int(5)
,`iddetail` int(5)
,`idjadwal` int(5)
,`nama` varchar(50)
,`umur` int(3)
,`jk` enum('Pria','Wanita')
,`nokamar` int(3)
,`tipe_kamar` enum('Standar','Deluxe','Suite')
,`checkIn` timestamp
,`lama_tinggal` int(3)
,`checkOut` timestamp
);

-- --------------------------------------------------------

--
-- Table structure for table `jadwal_check`
--

CREATE TABLE `jadwal_check` (
  `idjadwal` int(5) NOT NULL,
  `checkIn` timestamp NOT NULL DEFAULT current_timestamp(),
  `lama_tinggal` int(3) NOT NULL,
  `checkOut` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `finished` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure for view `informasi`
--
DROP TABLE IF EXISTS `informasi`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `informasi`  AS SELECT `daftarnama`.`idnama` AS `idnama`, `detail_pesanan`.`iddetail` AS `iddetail`, `jadwal_check`.`idjadwal` AS `idjadwal`, `daftarnama`.`nama` AS `nama`, `daftarnama`.`umur` AS `umur`, `daftarnama`.`jk` AS `jk`, `detail_pesanan`.`nokamar` AS `nokamar`, `detail_pesanan`.`tipe_kamar` AS `tipe_kamar`, `jadwal_check`.`checkIn` AS `checkIn`, `jadwal_check`.`lama_tinggal` AS `lama_tinggal`, `jadwal_check`.`checkOut` AS `checkOut` FROM ((`daftarnama` join `detail_pesanan` on(`daftarnama`.`idnama` = `detail_pesanan`.`iddetail`)) join `jadwal_check` on(`detail_pesanan`.`iddetail` = `jadwal_check`.`idjadwal`)) ORDER BY `daftarnama`.`idnama` ASC, `detail_pesanan`.`iddetail` ASC, `jadwal_check`.`idjadwal` ASC ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `daftarnama`
--
ALTER TABLE `daftarnama`
  ADD PRIMARY KEY (`idnama`);

--
-- Indexes for table `detail_pesanan`
--
ALTER TABLE `detail_pesanan`
  ADD PRIMARY KEY (`iddetail`);

--
-- Indexes for table `jadwal_check`
--
ALTER TABLE `jadwal_check`
  ADD PRIMARY KEY (`idjadwal`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `daftarnama`
--
ALTER TABLE `daftarnama`
  MODIFY `idnama` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `detail_pesanan`
--
ALTER TABLE `detail_pesanan`
  MODIFY `iddetail` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jadwal_check`
--
ALTER TABLE `jadwal_check`
  MODIFY `idjadwal` int(5) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
