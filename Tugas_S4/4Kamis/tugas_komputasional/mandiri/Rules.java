package mandiri;

public class Rules {
    double [] alphaPred = new double[3];
    double [] z = new double[3];
    double hasil;

    celcius dCel = new celcius();
    fahrenheit dFan = new fahrenheit();
    kelvin dKel = new kelvin();
    pengukuran derajat = new pengukuran();
    proses prod = new proses();

    void pilihan(double a) {
        // hasil alpha predikat untuk Rule 1
        alphaPred[0] = dCel.fuzzyDingin(a);
        // hasil alpha predikat untuk Rule 2
        alphaPred[1] = dCel.fuzzySedang(a);
        // hasil alpha predikat untuk Rule 3
        alphaPred[2] = dCel.fuzzyPanas(a);
    }

    void nilai_z() {
        z[0] = dCel.fuzzyDingin(alphaPred[0]);
        z[1] = dCel.fuzzySedang(alphaPred[1]);
        z[2] = dCel.fuzzyPanas(alphaPred[2]);
    }

    double statement1() {
        nilai_z();
        hasil = alphaPred[0];
        return hasil;
    }
    double statement2() {
        nilai_z();
        hasil = alphaPred[1];
        return hasil;
    }
    double statement3() {
        nilai_z();
        hasil = alphaPred[2];
        return hasil;
    }
}
