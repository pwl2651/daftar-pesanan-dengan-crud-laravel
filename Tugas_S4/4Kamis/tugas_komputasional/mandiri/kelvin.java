package mandiri;

public class kelvin {
    // Fungsi keanggotaan untuk suhu dingin
    public static double fuzzyDingin(double suhu) {
        if (suhu <= 273.15) {
            return 1.0;
        } else if (suhu > 273.15 && suhu < 278.15) {
            return (278.15 - suhu) / 5.0;
        } else {
            return 0.0;
        }
    }

    // Fungsi keanggotaan untuk suhu sedang
    public static double fuzzySedang(double suhu) {
        if (suhu >= 273.15 && suhu <= 293.15) {
            return 1.0;
        } else if (suhu > 263.15 && suhu < 273.15) {
            return (suhu - 263.15) / 10.0;
        } else if (suhu > 293.15 && suhu < 303.15) {
            return (303.15 - suhu) / 10.0;
        } else {
            return 0.0;
        }
    }

    // Fungsi keanggotaan untuk suhu panas
    public static double fuzzyPanas(double suhu) {
        if (suhu >= 293.15) {
            return 1.0;
        } else if (suhu > 283.15 && suhu < 293.15) {
            return (suhu - 283.15) / 10.0;
        } else {
            return 0.0;
        }
    }
}
