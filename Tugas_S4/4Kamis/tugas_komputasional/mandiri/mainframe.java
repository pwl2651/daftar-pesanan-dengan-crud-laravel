package mandiri;

import javax.swing.*;
import java.awt.event.*;
import java.awt.*;

public class mainframe extends JFrame implements ActionListener {
    ImageIcon imageIcon = new ImageIcon(getClass().getResource("tugasKecerdasan.png"));
    Image image = imageIcon.getImage();
    Image skalaFoto = image.getScaledInstance(300, 200, Image.SCALE_SMOOTH);
    ImageIcon foto = new ImageIcon(skalaFoto);
    JTextField tf_celcius, tf_pilihan, tf_keanggotaan;
    JLabel lb_celcius, lb_pilihan,lb_enum, lb_keanggotaan, keterangan;
    JButton hitung;

    mainframe() {

        keterangan = new JLabel(foto);
        keterangan.setBounds(5, 5, 300, 200);

        lb_celcius = new JLabel("Derajat Suhu");
        lb_celcius.setBounds(350, 20, 150, 30);

        tf_celcius = new JTextField();
        tf_celcius.setBounds(650, 20, 150, 30);

        lb_pilihan = new JLabel("Pilihan Keanggotaan");
        lb_pilihan.setBounds(350, 70, 200, 30);

        tf_pilihan = new JTextField();
        tf_pilihan.setBounds(650, 70, 150, 30);

        lb_enum = new JLabel("(1.Dingin 2.Sedang 3.Panas)");
        lb_enum.setBounds(350, 90, 200, 30);

        lb_keanggotaan = new JLabel("Derajat Keanggotaan");
        lb_keanggotaan.setBounds(350, 140, 300, 30);

        tf_keanggotaan = new JTextField();
        tf_keanggotaan.setEditable(false);
        tf_keanggotaan.setBounds(650, 140, 150, 30);
        
        hitung = new JButton("Hitung");
        hitung.setBounds(350, 300, 100, 30);
        hitung.addActionListener(this);
        hitung.setActionCommand("Hitung");

        add(keterangan);
        add(lb_celcius); add(tf_celcius);
        add(lb_enum);
        add(lb_pilihan); add(tf_pilihan);
        add(lb_keanggotaan);   add(tf_keanggotaan);
        add(hitung);

        setTitle("Konversi dan keanggotaan suhu");
        setSize(1000,400);
        setLayout(null);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public void actionPerformed(ActionEvent e) {
            // TODO Auto-generated method stub
            String klik = e.getActionCommand();
            if (klik.equals("Hitung")) {
                double suhuCelcius = Double.parseDouble(tf_celcius.getText());
                try{

                Rules rule = new Rules();
                rule.pilihan(suhuCelcius);
                int pilihanKeanggotaan = Integer.parseInt(tf_pilihan.getText());
                if (pilihanKeanggotaan==1){
                    tf_keanggotaan.setText(rule.statement1()+"");
                }else if(pilihanKeanggotaan==2){
                tf_keanggotaan.setText(rule.statement2()+"");
                }else if(pilihanKeanggotaan==3){
                tf_keanggotaan.setText(rule.statement3()+"");
                }
                }catch(NumberFormatException angka){
                    System.out.println("Invalid! (Harus menggunakan angka)");
                }
            }
    }
    
    public static void main (String args[]) {
        new mainframe();
    }
    
}
