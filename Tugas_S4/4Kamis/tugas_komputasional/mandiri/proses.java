package mandiri;

import java.util.Scanner;

public class proses {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // Input variabel suhu
        System.out.print("Masukkan suhu ruangan: ");
        double suhu = scanner.nextDouble();

        // Logika Fuzzy
        double suhuDingin = fuzzyDingin(suhu);
        double suhuNyaman = fuzzyNyaman(suhu);
        double suhuHangat = fuzzyHangat(suhu);

        // Output
        System.out.println("Keanggotaan Suhu Dingin: " + suhuDingin);
        System.out.println("Keanggotaan Suhu Nyaman: " + suhuNyaman);
        System.out.println("Keanggotaan Suhu Hangat: " + suhuHangat);

        scanner.close();
    }

    // Fungsi keanggotaan untuk suhu dingin
    public static double fuzzyDingin(double suhu) {
        if (suhu <= 18) {
            return 1.0;
        } else if (suhu > 18 && suhu < 22) {
            return (22 - suhu) / 4;
        } else {
            return 0.0;
        }
    }

    // Fungsi keanggotaan untuk suhu nyaman
    public static double fuzzyNyaman(double suhu) {
        if (suhu >= 20 && suhu <= 24) {
            return 1.0;
        } else if (suhu > 18 && suhu < 20) {
            return (suhu - 18) / 2;
        } else if (suhu > 24 && suhu < 26) {
            return (26 - suhu) / 2;
        } else {
            return 0.0;
        }
    }

    // Fungsi keanggotaan untuk suhu hangat
    public static double fuzzyHangat(double suhu) {
        if (suhu >= 22) {
            return 1.0;
        } else if (suhu > 18 && suhu < 22) {
            return (suhu - 18) / 4;
        } else {
            return 0.0;
        }
    }
}
