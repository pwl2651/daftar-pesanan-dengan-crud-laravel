package mandiri;

public class pengukuran {
    void derajat(double celcius,double fahrenheit, double kelvin){
        //celcius ke fahrenheit
        double C2F = (celcius * 9 / 5) + 32;
        //celcius ke kelvin
        double C2K = celcius + 273.15;
        //fahrenheit ke celcius
        double F2C = (fahrenheit - 32) * 5 / 9;
        //fahrenheit ke kelvin
        double F2K = (fahrenheit + 459.67) * 5 / 9;
        //kelvin ke celcius
        double K2C = kelvin - 273.15;
        //kelvin ke fahrenheit
        double K2F = (kelvin * 9 / 5) - 459.67;
    }
}
