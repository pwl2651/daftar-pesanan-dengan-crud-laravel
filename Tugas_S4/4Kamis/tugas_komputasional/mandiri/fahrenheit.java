package mandiri;

public class fahrenheit {
    // Fungsi keanggotaan untuk suhu dingin
    public static double fuzzyDingin(double suhu) {
        if (suhu <= 50) {
            return 1.0;
        } else if (suhu > 50 && suhu < 60) {
            return (60 - suhu) / 10.0;
        } else {
            return 0.0;
        }
    }

    // Fungsi keanggotaan untuk suhu sedang
    public static double fuzzySedang(double suhu) {
        if (suhu >= 50 && suhu <= 80) {
            return 1.0;
        } else if (suhu > 40 && suhu < 50) {
            return (suhu - 40) / 10.0;
        } else if (suhu > 80 && suhu < 90) {
            return (90 - suhu) / 10.0;
        } else {
            return 0.0;
        }
    }

    // Fungsi keanggotaan untuk suhu panas
    public static double fuzzyPanas(double suhu) {
        if (suhu >= 80) {
            return 1.0;
        } else if (suhu > 70 && suhu < 80) {
            return (suhu - 70) / 10.0;
        } else {
            return 0.0;
        }
    }
}
